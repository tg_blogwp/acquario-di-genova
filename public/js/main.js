
var Utils = function () {

    (function () {
        var lastTime = 0;
        var vendors = ['ms', 'moz', 'webkit', 'o'];
        for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
            window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
            window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame']
                                       || window[vendors[x] + 'CancelRequestAnimationFrame'];
        }

        if (!window.requestAnimationFrame)
            window.requestAnimationFrame = function (callback, element) {
                var currTime = new Date().getTime();
                var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                var id = window.setTimeout(function () { callback(currTime + timeToCall); },
                  timeToCall);
                lastTime = currTime + timeToCall;
                return id;
            };

        if (!window.cancelAnimationFrame)
            window.cancelAnimationFrame = function (id) {
                clearTimeout(id);
            };
    }());

    var transformProperty = function detectTransformProperty() {
        var transformProperty = 'transform',
            safariPropertyHack = 'webkitTransform';
        if (typeof document.body.style[transformProperty] !== 'undefined') {
            ['webkit', 'moz', 'o', 'ms'].every(function (prefix) {
                var e = '-' + prefix + '-transform';
                if (typeof document.body.style[e] !== 'undefined') {
                    transformProperty = e;
                    return false;
                }
                return true;
            });
        } else if (typeof document.body.style[safariPropertyHack] !== 'undefined') {
            transformProperty = '-webkit-transform';
        } else {
            transformProperty = undefined;
        }
        return transformProperty;
    }();

    function Utils() {

    }

    Utils.transformProperty = transformProperty;

    return Utils;

}();
/*global dynamics*/

var app = angular.module('acquario', ['ngRoute']);

app.constant('ROUTES', window.sections ? sections : []);
app.constant('VISITS', window.visits ? visits : []);
app.constant('EXPERIENCES', window.experiences ? experiences : []);
app.constant('FARES', window.fares ? fares : []);

app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    
    $routeProvider.when('/:section', {
        // DEFINE ROUTE
    }).when('/:section/:chapter', {
        // DEFINE ROUTE
    });

    // $routeProvider.otherwise('/404');

    // HTML5 MODE url writing method (false: #/anchor/use, true: /html5/url/use)
    $locationProvider.html5Mode(false);

}]);

app.run(['$rootScope', 'ROUTES', 'EXPERIENCES', function ($rootScope, ROUTES, EXPERIENCES) {

    $rootScope.$on('$routeChangeSuccess', function ($e, current, previous) {
        // console.log('$routeChangeSuccess', current.params);
        $rootScope.section = current.params.section;
        $rootScope.chapter = current.params.chapter;
        angular.forEach(ROUTES, function (value, index) {
            if (value.name == $rootScope.section) {
                value.chapter = $rootScope.chapter;
            }
        });
    });

    $rootScope.isBeforeActiveIndex = function ($index) {
        var is = false;
        angular.forEach(ROUTES, function (value, index) {
            // console.log(value, $rootScope.section, index, $index);
            if (value.name == $rootScope.section && index > $index) {
                is = true;
            }
        });
        return is;
    };

    $rootScope.isSubActive = function ($section, $chapter, $index) {
        if ($rootScope.section == $section && $rootScope.chapter == $chapter) {
            return true;
        } else {
            var is = false;
            angular.forEach(ROUTES, function (value, index) {
                if (value.name == $section) {
                    is = (value.chapter ? value.chapter == $chapter : $index == 0);
                }
            });
            return is;
        }
    };

    $rootScope.slickOptions = {
        spots: {
            dots: true,
            arrows: false,
            draggable: true,
            autoplay: false,
            infinite: false,
            lazyLoad: 'progressive',
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [{
                breakpoint: 1023,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    variableWidth: true
                }
            }]
        },
        more: {
            dots: false,
            arrows: true,
            draggable: true,
            autoplay: false,
            infinite: true,
            lazyLoad: 'progressive',
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [{
                breakpoint: 1023,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
        }
    };

    $rootScope.broadcast = function (event, params) {
        $rootScope.$broadcast(event, params);
    }
    $rootScope.menuMobileOpen = function () {
        $rootScope.menuMobileOpened = true;
    };
    $rootScope.menuMobileClose = function () {
        $rootScope.menuMobileOpened = false;
    };
    $rootScope.menuOpen = function () {
        $rootScope.menuOpened = true;
    };
    $rootScope.menuClose = function () {
        $rootScope.menuOpened = false;
    };
    $rootScope.menuToggle = function () {
        $rootScope.menuOpened = !$rootScope.menuOpened;
    };
    $rootScope.gotoTickets = function () {
        window.location.href = 'tickets.html';
    };

    $rootScope.doShowExperience = function (dir) {
        if (dir > 0) {
            $rootScope.experienceIndex ++;
            if ($rootScope.experienceIndex >= EXPERIENCES.length) {
                $rootScope.experienceIndex = 0;
            }
        } else if (dir < 0) {
            $rootScope.experienceIndex --;
            if ($rootScope.experienceIndex < 0) {
                $rootScope.experienceIndex = EXPERIENCES.length - 1;
            }
        } else {
            $rootScope.experienceIndex = 0;
        }
    };
    $rootScope.isExperienceVisible = function (index) {
        return $rootScope.experienceIndex === index;
    };

    $rootScope.doBuyTicket = function (id) {
        alert('doBuyTicket ' + id);
    };

    /*
    // FONT PRELOADER
    setTimeout(function () {

        var font400 = new FontFaceObserver("Proxima Nova", {
            weight: 400
        });
        var font500 = new FontFaceObserver("Proxima Nova", {
            weight: 500
        });
        var font600 = new FontFaceObserver("Proxima Nova", {
            weight: 600
        });
        var font700 = new FontFaceObserver("Proxima Nova", {
            weight: 700
        });
        var font900 = new FontFaceObserver("Proxima Nova", {
            weight: 900
        });
        var font400Serif = new FontFaceObserver("Adelle", {
            weight: 400
        });
        Promise.all([
            font400.check(),
            font500.check(null, 1000),
            font600.check(null, 1000),
            font700.check(null, 1000),
            font900.check(null, 1000),
            font400Serif.check(),
        ]).then(function () {

            initWowJs();

            setTimeout(function () {
                document.documentElement.className += " fonts-loaded";
            }, 1);

        }, function () {
            document.documentElement.className += " fonts-timeout";
        });

    }, 0);

    */

    function initWowJs() {

        // WOW ANIMATION
        new WOW({
            boxClass: 'wow',
            animateClass: 'animated',
            mobile: false,
            live: true,
            offset: 0,

        }).init();

    };

    initWowJs();

}]);

/*global app, dynamics*/

app.directive('pageNav', ['$window', '$location', '$timeout', function($window, $location, $timeout) {
    var position = { 
        y: window.scrollY
    };
    return {
        restrict: 'A',
        link: function (scope, element, attributes, model) {    
            function scrollTo() {
                var targets = document.querySelectorAll('a[name="' + attributes.href.replace('#', '') + '"]');
                if (targets.length == 0) {
                    return;
                }   
                var rect = targets[0].getBoundingClientRect(); 
                var y = rect.top + window.scrollY - 120;   
                if (y !== position.y) {
                    dynamics.animate(position, {
                        "y": y
                    }, {
                        type: dynamics.bezier,
                        points: [{
                            "x":0,
                            "y":0,
                            "cp":[{
                                "x":0.121,"y":0.883
                            }]
                        },{
                            "x":1,
                            "y":1,
                            "cp":[{
                                "x":0.402,"y":1.023
                            }]
                        }], 
                        change: function (position) {
                            // console.log('scroll', position.y);
                            window.scrollTo(0, position.y);    
                        }
                    });
                }
            }
            function onClick(e) {
                // console.log('pageNav.onClick', attributes.href);
                window.location.hash = attributes.href;                
                // $timeout(scrollTo, 1);
                e.stopImmediatePropagation();
                e.preventDefault();
            };
            function addListeners() {
                element.on('click', onClick);
            };
            function removeListeners() {
                element.off('click', onClick);
            };
            scope.$on('$destroy', function () {
                removeListeners();
            });
            addListeners();
        }
    }
}]);

app.directive('slick', ['$timeout', function ($timeout) {
    return {
        scope: {
            slick: '=',
            slickOptions: '=?',
        },
        link: function (scope, element, attributes, model) {
            scope.slickOptions = scope.slickOptions || {};
            scope.slickOptions.slickGoTo = function (i, animated) {
                scope.slickOptions.initialSlide = i;
            };
            scope.slickOptions.onChange = scope.slickOptions.onChange || new Function;
            var initOptions = {
                infinite: attributes.arrows == "true" ? true : false,
                dots: attributes.arrows == "true" ? false : true,
                autoplay: false,
                arrows: attributes.arrows == "true" ? true : false,
                vertical: false,
                verticalSwiping: false,
                initialSlide: scope.slickOptions.initialSlide || 0,
            }
            initOptions = angular.extend(initOptions, scope.slickOptions);
            $timeout(function () {
                init();
            }, 100);
            function init() {
                element.slick(initOptions);
                scope.slickOptions.slickGoTo = function (i, animated) {
                    if (i !== scope.current) {
                        element.slick('slickGoTo', i, animated);
                    }
                };
            }            
            function onBeforeChange(event, slick, currentSlide, nextSlide) {
                $timeout(function () {
                    scope.current = nextSlide;
                    scope.previous = currentSlide;
                }, 1);
                scope.slickOptions.onChange(nextSlide, currentSlide);
            }
            function addListeners() {
                element.on('beforeChange', onBeforeChange);
            };
            function removeListeners() {
                element.off('beforeChange', onBeforeChange);
            };
            scope.$on('$destroy', function () {
                removeListeners();
            });
            addListeners();
        }
    }
}]);

app.directive('stickyItem', ['$window', '$document', '$timeout', function ($window, $document, $timeout) {
    var getNow = Date.now || function () {
        return new Date().getTime();
    };
    function throttle(func, wait, options) {
        var context, args, result;
        var timeout = null;
        var previous = 0;
        if (!options) options = {};
        var later = function () {
            previous = options.leading === false ? 0 : getNow();
            timeout = null;
            result = func.apply(context, args);
            if (!timeout) context = args = null;
        };
        return function () {
            var now = getNow();
            if (!previous && options.leading === false) previous = now;
            var remaining = wait - (now - previous);
            context = this;
            args = arguments;
            if (remaining <= 0 || remaining > wait) {
                if (timeout) {
                    clearTimeout(timeout);
                    timeout = null;
                }
                previous = now;
                result = func.apply(context, args);
                if (!timeout) context = args = null;
            } else if (!timeout && options.trailing !== false) {
                timeout = setTimeout(later, remaining);
            }
            return result;
        };
    }

    return {
        restrict: 'C',
        link: function (scope, element, attributes, model) {

            var vertical = element; //.css({ 'display': 'inline-block', 'position': 'relative', 'z-index': 1000 });
            var stickyVertical = vertical[0];
            var horizontal = angular.element(stickyVertical.querySelector('.sticky-horizontal')); //.css({ 'display': 'inline-block', 'position': 'relative' });
            var stickyHorizontal = horizontal[0];
            var buttons = document.querySelectorAll('.sticky-buttons');

            if (buttons.length == 0) {
                vertical.css({ 'display': 'none' });
                return;
            }

            var tx = 0, ty = 0;
            $.each(buttons, function (index, item) {
                if (index !== buttons.length - 1) {
                    $(this).html($(stickyHorizontal).html()).css({ 'pointer-events': 'none', 'position': 'relative' })/*.css({ 'pointer-events': 'none', 'display': 'inline-block', 'position': 'relative', 'opacity': .2 })*/.find('.sticky').removeClass('last');
                }
            });
            dynamics.css(stickyHorizontal, { opacity: 0 });
            function isRectInViewport(rect, st, sl, width, height) {
                return (
                    rect.top >= st &&
                    rect.top + height <= st + (window.innerHeight || document.documentElement.clientHeight) &&
                    rect.left >= sl &&
                    rect.left + width <= sl + (window.innerWidth || document.documentElement.clientWidth)
                );
            }
            var current, previous;
            function doCheck(immediate) {
                var min = Number.POSITIVE_INFINITY;
                var rect = vertical.offset();
                rect.top -= ty;
                var st = $(window).scrollTop(), sl = $(window).scrollLeft();
                var goRect = null, centerY = st + (window.innerHeight || document.documentElement.clientHeight) / 2;
                $.each(buttons, function (index, item) {
                    var ir = $(item).offset(), bound = vertical[0].getBoundingClientRect();
                    if (index === buttons.length - 1) {
                        ir.top -= ty;
                    }
                    var isInViewPort = isRectInViewport(ir, st, sl, bound.width, bound.height);
                    var distance = Math.abs(ir.top - centerY);
                    if (isInViewPort && distance < min) {
                        min = distance;
                        goRect = ir;
                        goRect.index = index;
                        goRect.distance = distance;
                        goRect.width = bound.width;
                        goRect.height = bound.height;
                        if (immediate === true) {
                            previous = index;
                        } else {
                            current = index;
                        }
                        if (index === buttons.length - 1) {
                            vertical.find('.sticky').addClass('last');
                        } else {
                            vertical.find('.sticky').removeClass('last');
                        }
                    }
                });
                if (goRect) {
                    if (immediate === true) {
                        ty = goRect.top - rect.top;
                        tx = goRect.left - rect.left;
                        dynamics.css(stickyVertical, { translateY: ty });
                        dynamics.css(stickyHorizontal, { translateX: tx });
                        setTimeout(function () {
                            dynamics.animate(stickyHorizontal, { opacity: 1 }, {
                                type: dynamics.easeOut,
                                duration: 150,
                            });
                        }, 1);
                    } else if (current !== previous) {
                        // console.log(goRect.index, current, 'distance', goRect.distance, 'rect.top', rect.top, 'top', goRect.top, 'left', goRect.left, 'width', goRect.width, 'height', goRect.height);
                        previous = current;
                        dynamics.css(stickyHorizontal, { opacity: 0.2 });
                        ty = goRect.top - rect.top;
                        tx = goRect.left - rect.left;
                        dynamics.animate(stickyVertical, { translateY: ty }, {
                            type: dynamics.bezier,
                            points: [{ "x": 0, "y": 0, "cp": [{ "x": 0.491, "y": 0.025 }] }, { "x": 1, "y": 1, "cp": [{ "x": 0.491, "y": 1.023 }] }],
                            duration: 700,
                            change: function () {
                                var style = vertical.attr('style'), value = 0;
                                if (style.indexOf('matrix3d') !== -1) {
                                    value = style.split('matrix3d(')[1].split(')')[0].split(',')[13];
                                } else if (style.indexOf('matrix') !== -1) {
                                    value = style.split('matrix(')[1].split(')')[0].split(',')[5];
                                } else if (style.indexOf('translateY') !== -1) {
                                    value = style.split('translateY(')[1].split(')')[0];
                                } else if (style.indexOf('translate') !== -1) {
                                    value = style.split('translate(')[1].split(')')[0].split(',')[1];
                                }
                                ty = parseFloat(value);
                            },
                        });
                        dynamics.animate(stickyHorizontal, { translateX: tx }, {
                            type: dynamics.bezier,
                            points: [{ "x": 0, "y": 0, "cp": [{ "x": 0.228, "y": 0.865 }] }, { "x": 1, "y": 1, "cp": [{ "x": 0.795, "y": 0.095 }] }],
                            duration: 700,
                            complete: function () {
                                dynamics.animate(stickyHorizontal, { opacity: 1 }, {
                                    type: dynamics.easeOut,
                                    duration: 150,
                                });
                            }
                        });
                    }
                }
            }

            setTimeout(function () {
                doCheck(true);
                addListeners();
            }, 2000);

            function onResize(e) {
                doCheck(true);
            };
            function _onScroll(e) {
                doCheck();
            };
            var onScroll = throttle(_onScroll, 500);
            function addListeners() {
                angular.element($window).on('resize', onResize);
                angular.element($window).on('scroll', onScroll);
            };
            function removeListeners() {
                angular.element($window).off('resize', onResize);
                angular.element($window).off('scroll', onScroll);
            };
            scope.$on('$destroy', function () {
                removeListeners();
            });
            setTimeout(function () {
                doCheck(true);
                addListeners();
            }, 2000);
        }
    }

}]);

app.directive('background', ['$window', '$timeout', function ($window, $timeout) {

    function Style() {
        this.props = {
            scale: 1,
            hoverScale: 1,
            currentScale: 1,
        }
    }
    Style.prototype = {
        set: function (element) {
            var styles = [];
            angular.forEach(this, function (value, key) {
                if (key !== 'props')
                    styles.push(key + ':' + value);
            });
            element.style.cssText = styles.join(';') + ';';
        },
        transform: function (transform) {
            this[Utils.transformProperty] = transform;
        },
        transformOrigin: function (x, y) {
            this[Utils.transformProperty + '-origin-x'] = (Math.round(x * 1000) / 1000) + '%';
            this[Utils.transformProperty + '-origin-y'] = (Math.round(y * 1000) / 1000) + '%';
        },
    };

    var ischrome = /chrome/.test(navigator.userAgent.toLowerCase());

    return {
        restrict: 'A',
        link: function (scope, element, attributes, model) {

            if (!ischrome) {
                element.addClass('background-fixed');
                return;
            }

            var current = -1, hover = false, aKey, ew, eh, mx = 0, my = 0, rx = 0, ry = 0, rowsLength = 60;
            var sx, sy, sw, sh;

            var videos = element.find('video');
            var styles = [], style = new Style();

            function redraw(time) {
                rx += (mx - rx) / 8;
                ry += (my - ry) / 8;
                style.transformOrigin(50, 100);
                // style.transform('rotateY(' + (rx * 4) + 'deg) translateZ(-100px) scale(1.2)'); // rotateX(' + (ry * 4) + 'deg)'); //  scale(3)
                ischrome ?
                    style.transform('rotateY(' + (rx * 6) + 'deg) translateZ(-10000px) translateY(7000px) scale(20)') :
                    null;
                style.set(element[0]);
                angular.forEach(videos, function (video, i) {
                    var ss = styles[i];
                    /*
                    if (hover) {
                        ss.props.currentScale += (ss.props.hoverScale - ss.props.currentScale) / 8;
                    } else {
                        ss.props.currentScale += (ss.props.scale - ss.props.currentScale) / 8;
                    }
                    */
                    ss.props.currentScale = ss.props.scale;
                    ss.transformOrigin(50, 100);
                    ss.transform('translate3d(-50%,0,0) scale(' + ss.props.currentScale + ',' + ss.props.currentScale + ')');
                    ss.set(video);
                });
            }

            function play() {
                function loop(time) {
                    redraw(time);
                    aKey = window.requestAnimationFrame(loop, element);
                }
                if (!aKey) {
                    loop();
                }
            }

            function pause() {
                if (aKey) {
                    window.cancelAnimationFrame(aKey);
                    aKey = null;
                }
            }

            function onResize() {
                ew = Math.min(1600, element[0].offsetWidth + 2) - 100;
                // ew = element[0].offsetWidth + 2 - 100;
                eh = element[0].offsetHeight + 2;
                er = ew / eh;
                var scale;
                angular.forEach(videos, function (video, i) {
                    if (video.offsetWidth) {
                        var iw = video.offsetWidth;
                        var ih = video.offsetHeight;
                        console.log(iw, ih);
                        var ir = iw / ih;
                        if (ir > er) {
                            scale = eh / ih;
                            // sh = ih;
                            // sw = ih * er;
                        } else {
                            scale = ew / iw;
                            // sw = iw;
                            // sh = sw / er;
                        }
                        // sx = (iw - sw) / 2;
                        // sy = (ih - sh) / 2;
                        var style = styles[i];
                        style.props.scale = scale * 1.1;
                        style.props.hoverScale = scale * 1;
                    }
                });
            }

            function onMove(e) {
                mx = Math.min(1, Math.max(0, e.clientX / ew)) - 0.5;
                my = Math.min(1, Math.max(0, e.clientY / eh)) - 0.5;
            }

            /*
            function onClick() {
                if (aKey) {
                    pause()
                } else {
                    Next();
                    play();
                }
            }
            */

            function addListeners() {
                // element.on('click', onClick);
                angular.element($window)
                    .on('resize', onResize)
                    .on('mousemove', onMove);
            };
            function removeListeners() {
                // element.off('click', onClick);
                angular.element($window)
                    .off('resize', onResize)
                    .off('mousemove', onMove);
            };
            scope.$on('$destroy', function () {
                removeListeners();
            });

            function Init() {
                var i = 0;
                angular.forEach(videos, function (video, i) {
                    styles.push(new Style());
                    /*
                    var img = new Image();
                    img.i = i;
                    img.onload = function () {
                        if (this.i === 0) {
                            Next();
                        }
                    }
                    img.src = video.src;
                    */
                });
            }

            var to;
            function Next() {
                current++;
                if (current == videos.length) {
                    current = 0;
                }
                onShow(current);
                to = setTimeout(Next, 5000);
            }

            function onShow() {
                angular.forEach(videos, function (video, i) {
                    var videoElement = angular.element(video);
                    videoElement.removeClass('hover active');
                    if (i === current) {
                        videoElement.addClass(hover ? 'hover' : 'active');
                    }
                });
            }

            scope.$on('onHeaderOver', function ($scope, index) {
                to ? clearTimeout(to) : null;
                current = index;
                hover = true;
                onResize();
                onShow();
                /*
                angular.forEach(canvases, function (canvas, i) {
                    var canvasElement = angular.element(canvas);
                    canvasElement.removeClass('active');
                });
                */
            });

            scope.$on('onHeaderOut', function ($scope, index) {
                to ? clearTimeout(to) : null;
                hover = false;
                onResize();
                onShow();
                to = setTimeout(Next, 5000);
                /*
                angular.forEach(canvases, function (canvas, i) {
                    var canvasElement = angular.element(canvas);
                    if (i === current) {
                        canvasElement.addClass('active');
                    } else {
                        canvasElement.removeClass('active');
                    }
                });
                */
            });

            Init();
            onResize();
            Next();
            play();
            addListeners();

        },
    };
}]);

app.directive('ticketsFixed', ['$window', function ($window) {
    return {
        restrict: 'A',
        link: function (scope, element, attributes, model) {

            var sticky = element[0];

            function onScroll() {
                var scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
                var from = 360, to = 1000;
                if (scrollTop > from && scrollTop < to) {
                    dynamics.stop(sticky);
                    dynamics.animate(sticky, { translateY: scrollTop - from }, { type: dynamics.easeIn, friction: 300, duration: 200, delay: 0 });
                } else {
                    dynamics.stop(sticky);
                    dynamics.animate(sticky, { translateY: 0 }, { type: dynamics.easeIn, friction: 300, duration: 200, delay: 0 });
                }
            };
            function addListeners() {
                angular.element($window).on('scroll', onScroll);
            };
            function removeListeners() {
                angular.element($window).off('scroll', onScroll);
            };
            scope.$on('$destroy', function () {
                removeListeners();
            });
            addListeners();
            onScroll();
        }
    }
}]);

app.factory('Matrix', [function () {
    var transform = { name: 'transform', key: 'transform', matrix: null };
    var is3dSupported = function isSupported3d() {
        if (!window.getComputedStyle) {
            return false;
        }
        transform.matrix = window.WebKitCSSMatrix || window.MozCSSMatrix || window.MsCSSMatrix || window.OCSSMatrix || window.CSSMatrix;
        if (!transform.matrix) {
            return false;
        }
        var element = document.createElement('div'),
            has = false,
            properties = {
                webkitTransform: '-webkit-transform',
                OTransform: '-o-transform',
                msTransform: '-ms-transform',
                MozTransform: '-moz-transform',
                transform: 'transform'
            };

        document.body.insertBefore(element, null);
        for (var p in properties) {
            if (element.style[p] !== undefined) {
                element.style[p] = "translate3d(1px,1px,1px)";
                has = window.getComputedStyle(element).getPropertyValue(properties[p]);
                transform.name = p;
                transform.key = properties[p];
                break;
            }
        }
        document.body.removeChild(element);
        return (has !== undefined && has.length > 0 && has !== "none");
    }(transform);
    var decompositionTypeEnum = {
        QRLike: 1,
        LULike: 2,
    };
    function Matrix(element) {
        this.element = element || document.createElement('div');
        this.translate = { x: 0, y: 0, z: 0 };
        this.rotate = { x: 0, y: 0, z: 0 };
        this.scale = { x: 1, y: 1, z: 1 };
        this.skew = { x: 0, y: 0 };
        this.matrix = this.decompose();
        this.decompositionType = decompositionTypeEnum.QRLike;
        console.log('Matrix', is3dSupported, transform);
        this.get();
    }
    Matrix.prototype.decompose = function () {
        var computedStyle = window.getComputedStyle(this.element);
        var style = computedStyle[transform.name]; //.transform || computedStyle.WebkitTransform || computedStyle.MozTransform || computedStyle.msTransform || computedStyle.OTransform;
        if (!style) {
            this.matrix = new transform.matrix();
        } else {
            style = style.trim();
            this.matrix = new transform.matrix(style);
            if (style === 'none') {
                this.translate.x = this.translate.y = this.translate.z = 0;
                this.rotate.x = this.rotate.y = this.rotate.z = 0;
                this.scale.x = this.scale.y = this.scale.z = 1;
                this.skew.x = this.skew.y = 0;

            } else {
                var matrix3dRegexp = /matrix3d\((.*),(.*),(.*),(.*),(.*),(.*),(.*),(.*),(.*),(.*),(.*),(.*),(.*),(.*),(.*),(.*)\)/;
                var matrix3dMatch = matrix3dRegexp.exec(style);

                var matrix2dRegexp = /matrix\((.*),(.*),(.*),(.*),(.*),(.*)\)/;
                var matrix2dMatch = matrix2dRegexp.exec(style);

                if (matrix3dMatch !== null) {
                    /*****************/
                    /*** 3D MATRIX ***/
                    /*****************/
                    var matrix = new transform.matrix(style);
                    if (!matrix || isNaN(matrix.a) || isNaN(matrix.b) || isNaN(matrix.c) || isNaN(matrix.m41) || isNaN(matrix.m42) || isNaN(matrix.m43) || isNaN(matrix.m11)) {
                        throw "Could not catch CSSMatrix constructor for current browser, OR the constructor has returned a non-standard matrix object (need .a, .b, .c and mXX numerical properties to work)";
                    }
                    this.rotate.x = Math.asin(matrix.m22) * 180 / Math.PI; // deg;
                    this.rotate.y = Math.acos(matrix.m11) * 180 / Math.PI * (matrix.m13 > 0 ? -1 : 1); // deg
                    this.rotate.z = 0; // "TODO: Sorry, math people. I really need help here! Please implement this case for me. This will help you: http://9elements.com/html5demos/matrix3d/";
                    this.translate.x = matrix.m41; // px;
                    this.translate.y = matrix.m42; // px;
                    this.translate.z = matrix.m43; // px;
                    this.scale.x = matrix.m11; // if (matrix.m11 === matrix.m22 && matrix.m22 === matrix.m33)
                    this.scale.y = matrix.m11; // if (matrix.m11 === matrix.m22 && matrix.m22 === matrix.m33)
                    this.scale.z = matrix.m11; // if (matrix.m11 === matrix.m22 && matrix.m22 === matrix.m33)
                    this.skew.x = 0;
                    this.skew.y = 0;

                } else if (matrix2dMatch !== null) {
                    /*****************/
                    /*** 2D MATRIX ***/
                    /*****************/
                    var a = parseFloat(matrix2dMatch[1]);
                    var b = parseFloat(matrix2dMatch[2]);
                    var c = parseFloat(matrix2dMatch[3]);
                    var d = parseFloat(matrix2dMatch[4]);
                    var e = parseFloat(matrix2dMatch[5]);
                    var f = parseFloat(matrix2dMatch[6]);

                    var delta = a * d - b * c;

                    this.rotate.x = 0; // deg;
                    this.rotate.y = 0; // deg;
                    this.rotate.z = 0; // deg;

                    this.translate.x = e; // px;
                    this.translate.y = f; // px;
                    this.translate.z = 0; // px;

                    this.scale.x = 1;
                    this.scale.y = 1;
                    this.scale.z = 1;

                    this.skew.x = 0;
                    this.skew.y = 0;

                    if (this.decompositionType === decompositionTypeEnum.QRLike) {
                        // Apply the QR-like decomposition.
                        if (a != 0 || b != 0) {
                            var r = Math.sqrt(a * a + b * b);
                            this.rotate.z = (b > 0 ? Math.acos(a / r) : -Math.acos(a / r));
                            this.scale.x = r;
                            this.scale.y = delta / r;
                            this.skew.x = Math.atan((a * c + b * d) / (r * r));
                            this.skew.y = 0;
                        } else if (c != 0 || d != 0) {
                            var s = Math.sqrt(c * c + d * d);
                            this.rotate.z = Math.PI / 2 - (d > 0 ? Math.acos(-c / s) : -Math.acos(c / s));
                            this.scale.x = delta / s;
                            this.scale.y = s;
                            this.skew.x = 0;
                            this.skew.y = Math.atan((a * c + b * d) / (s * s));
                        } else { // a = b = c = d = 0
                            this.rotate.z = 0;
                            this.scale.x = 0;
                            this.scale.y = 0;
                            this.skew.x = 0;
                            this.skew.y = 0;
                        }
                    } else {
                        // Apply the LU-like decomposition.
                        if (a != 0) {
                            this.rotate.z = 0;
                            this.scale.x = a;
                            this.scale.y = delta / a;
                            this.skew.x = Math.atan(c / a);
                            this.skew.y = Math.atan(b / a);
                        } else if (b != 0) {
                            this.rotate.z = Math.PI / 2;
                            this.scale.x = b;
                            this.scale.y = delta / b;
                            this.skew.x = Math.atan(d / b);
                            this.skew.y = 0;
                        } else { // a = b = 0
                            this.rotate.z = 0;
                            this.scale.x = c;
                            this.scale.y = d;
                            this.skew.x = Math.PI / 4;
                            this.skew.y = 0;
                            this.scale.x = 0;
                            this.scale.y = 1;
                        }
                    }
                }
            }
        }
        return this.matrix;
    };
    Matrix.prototype.compose = function () {
        var matrix = new transform.matrix();
        if (is3dSupported) {
            matrix = matrix.translate(this.translate.x, this.translate.y, this.translate.z);
            matrix = matrix.scale(this.scale.x, this.scale.y, this.scale.z);
            matrix = matrix.rotate(this.rotate.x, this.rotate.y, this.rotate.z);
        } else {
            matrix = matrix.translate(this.translate.x, this.translate.y);
            matrix = matrix.scale(this.scale.x, this.scale.y);
            matrix = matrix.rotate(this.rotate.z);
        }
        return matrix;
    };
    Matrix.prototype.get = function () {
        this.decompose();
        console.log(this.toString());
    };
    Matrix.prototype.set = function () {
        var matrix = this.compose().toString();
        this.element.style[transform.name] = matrix;
        // console.log('Matrix.set', matrix);
        // console.log(this.toString());
    };
    Matrix.prototype.toString = function () {
        var t = this.translate;
        var r = this.rotate;
        var s = this.scale;
        return 'Matrix translate(' + t.x + ',' + t.y + ',' + t.z + ') rotate(' + r.x + ',' + r.y + ',' + r.z + ') scale(' + s.x + ',' + s.y + ',' + s.z + ')';
    };
    return Matrix;

}]);

app.directive('stickyHeader', ['$window', 'Matrix', function ($window, Matrix) {
    function detectTransformProperty() {        
        var transform = 'transform',
            safariPropertyHack = 'webkitTransform';
        if (typeof document.body.style[transform] !== 'undefined') {
            ['webkit', 'moz', 'o', 'ms'].every(function (prefix) {
                var e = '-' + prefix + '-transform';
                if (typeof document.body.style[e] !== 'undefined') {
                    transform = e;
                    return false;
                }
                return true;
            });
        } else if (typeof document.body.style[safariPropertyHack] !== 'undefined') {
            transform = '-webkit-transform';
        } else {
            transform = undefined;
        }
        return transform;
    }
    var transform = detectTransformProperty();
    return {
        restrict: 'A',
        link: function (scope, element, attributes, model) {
            if (window.navigator.userAgent.toLowerCase().indexOf('mobile') !== -1) {
                console.log('sticky-content.disabled');
                element.addClass('sticky-disabled');
                return;
            }
            // OPTIONS
            var stickySelector = attributes.stickyHeader !== undefined ? attributes.stickyHeader : '.sticky';
            var stickedClass = attributes.stickedClass !== undefined ? attributes.stickedClass : 'sticked';            
            var aKey,
                isSticked = false,
                scrollPointY = 0,
                scrollGapY = 0,
                scrollGapToY = 0,
                scrollDir = 0,
                stickyY = 0,
                elementY = 0,
                elementPreviousY = 0,
                elementRect,
                content,
                contentMatrix;
            function draw() {
                content = content || angular.element(element[0].querySelector(stickySelector));                
                contentMatrix = contentMatrix || new Matrix(content[0]);
                scrollGapY += (scrollGapToY - scrollGapY) / 4;
                if (elementY < scrollPointY && scrollDir == 1) {
                    stickyY = (elementY - scrollPointY - scrollGapY) * -1;
                    if (!isSticked) {
                        isSticked = true;
                        element.addClass(stickedClass);
                    }
                } else {
                    stickyY = 0;
                    if (isSticked) {
                        isSticked = false;
                        element.removeClass(stickedClass);
                    }
                }
                contentMatrix.translate.y = stickyY;
                contentMatrix.set();
                return;
            }
            function play() {
                function loop(time) {
                    draw(time);
                    aKey = window.requestAnimationFrame(loop, element);
                }
                if (!aKey) {
                    loop();
                }
            }
            function pause() {
                if (aKey) {
                    window.cancelAnimationFrame(aKey);
                    aKey = null;
                }
            }            
            function $onScroll() {
                elementRect = element[0].getBoundingClientRect();
                elementY = elementRect.top;       
                if (elementPreviousY >= elementY) {
                    // console.log('down')
                    scrollDir = 0;
                    scrollGapToY = -79;
                } else {
                    // console.log('up')
                    scrollDir = 1;
                    scrollGapToY = 0;
                }
                elementPreviousY = elementY;                
                play();
            }
            function onResize() {
                play();
            }
            var onScroll = $onScroll; // Utils.throttle($onScroll, 1000 / 60);
            function addListeners() {
                angular.element($window).on('resize', onResize);
                angular.element($window).on('scroll', onScroll);
            }
            function removeListeners() {
                angular.element($window).off('resize', onResize);
                angular.element($window).off('scroll', onScroll);
            }
            scope.$on('$destroy', function () {
                removeListeners();
                pause();
            });
            addListeners();
            $onScroll();
        }
    };
}]);

app.directive('sticky', ['$rootScope', '$window', '$timeout', 'Matrix', 'ROUTES', function ($rootScope, $window, $timeout, Matrix, ROUTES) {
    function detectTransformProperty() {
        var transform = 'transform',
            safariPropertyHack = 'webkitTransform';
        if (typeof document.body.style[transform] !== 'undefined') {
            ['webkit', 'moz', 'o', 'ms'].every(function (prefix) {
                var e = '-' + prefix + '-transform';
                if (typeof document.body.style[e] !== 'undefined') {
                    transform = e;
                    return false;
                }
                return true;
            });
        } else if (typeof document.body.style[safariPropertyHack] !== 'undefined') {
            transform = '-webkit-transform';
        } else {
            transform = undefined;
        }
        return transform;
    }
    var transform = detectTransformProperty();
    return {
        restrict: 'A',
        link: function (scope, element, attributes, model) {
            if (window.navigator.userAgent.toLowerCase().indexOf('mobile') !== -1) {
                console.log('sticky-content.disabled');
                element.addClass('sticky-disabled');
                return;
            }
            // OPTIONS
            var stickySelector = attributes.sticky !== undefined ? attributes.sticky : '.sticky';
            var stickedClass = attributes.stickedClass !== undefined ? attributes.stickedClass : 'sticked';
            // OPTIONS
            var aKey,
                isSticked = false,
                scrollPointY = 0,
                scrollPointToY = 0,
                scrollDir = 0,
                stickyY = 0,
                elementY = 0,
                elementPreviousY = 0,
                elementRect,
                content,
                contentMatrix;
            function draw() {
                content = content || angular.element(element[0].querySelector(stickySelector));                
                contentMatrix = contentMatrix || new Matrix(content[0]);
                scrollPointY += (scrollPointToY - scrollPointY) / 4;
                if (elementY < scrollPointY) {
                    stickyY = (elementY - scrollPointY) * -1;
                    if (!isSticked) {
                        isSticked = true;
                        element.addClass(stickedClass);
                    }
                } else {
                    stickyY = 0;
                    if (isSticked) {
                        isSticked = false;
                        element.removeClass(stickedClass);
                    }
                }
                contentMatrix.translate.y = stickyY;
                contentMatrix.set();
                return;
            }
            function play() {
                function loop(time) {
                    draw(time);
                    aKey = window.requestAnimationFrame(loop, element);
                }
                if (!aKey) {
                    loop();
                }
            }
            function pause() {
                if (aKey) {
                    window.cancelAnimationFrame(aKey);
                    aKey = null;
                }
            }
            function intersectRect(r1, r2) {
              return !( r2.top > r1.bottom || r2.bottom < r1.top );
            }
            function $onScroll() {
                elementRect = element[0].getBoundingClientRect();
                elementY = elementRect.top;     
                if (elementPreviousY >= elementY) {
                    // console.log('down')                    
                    scrollDir = 0;
                    scrollPointToY = 0;
                } else {
                    // console.log('up')
                    scrollDir = 1;
                    scrollPointToY = 79;
                }
                elementPreviousY = elementY;
                // CHECK ACTIVE ITEM
                var current = null;
                angular.forEach(ROUTES, function (value, index) {
                    var target = document.getElementById(value.name);
                    if (target) {
                        var rect1 = target.getBoundingClientRect();
                        var rect2 = { top: 120, bottom: window.innerHeight };
                        var intersect = intersectRect(rect1, rect2);
                        if (intersect && !current) {
                            current = value;
                            // console.log(value.name, rect1, rect2);
                        }
                    }
                });
                if (current && $rootScope.section !== current.name) {
                    var href = '#/' + current.name + (current.chapter ? '/' + current.chapter : '');
                    if (history.pushState) {
                        history.pushState(null, null, href);
                    }/* else {
                        window.location.hash = href;
                    }*/
                    $timeout(function() {
                        $rootScope.section = current.name;
                        $rootScope.chapter = current.chapter;
                    }, 1);                    
                }
                play();
            }
            function onResize() {
                play();
            }
            var onScroll = $onScroll; // Utils.throttle($onScroll, 1000 / 60);
            function addListeners() {
                angular.element($window).on('resize', onResize);
                angular.element($window).on('scroll', onScroll);
            }
            function removeListeners() {
                angular.element($window).off('resize', onResize);
                angular.element($window).off('scroll', onScroll);
            }
            scope.$on('$destroy', function () {
                removeListeners();
                pause();
            });
            addListeners();
            $onScroll();
        }
    };
}]);

app.directive('stickyFooter', ['$rootScope', '$window', '$timeout', 'Matrix', function ($rootScope, $window, $timeout, Matrix) {
    function detectTransformProperty() {
        var transform = 'transform',
            safariPropertyHack = 'webkitTransform';
        if (typeof document.body.style[transform] !== 'undefined') {
            ['webkit', 'moz', 'o', 'ms'].every(function (prefix) {
                var e = '-' + prefix + '-transform';
                if (typeof document.body.style[e] !== 'undefined') {
                    transform = e;
                    return false;
                }
                return true;
            });
        } else if (typeof document.body.style[safariPropertyHack] !== 'undefined') {
            transform = '-webkit-transform';
        } else {
            transform = undefined;
        }
        return transform;
    }
    var transform = detectTransformProperty();
    return {
        restrict: 'A',
        link: function (scope, element, attributes, model) {
            if (window.navigator.userAgent.toLowerCase().indexOf('mobile') !== -1) {
                console.log('sticky-content.disabled');
                element.addClass('sticky-disabled');
                return;
            }
            // OPTIONS
            var stickySelector = attributes.sticky !== undefined ? attributes.stickyFooter : '.sticky';
            var stickedClass = attributes.stickedClass !== undefined ? attributes.stickedClass : 'sticked';
            // OPTIONS
            var aKey,
                isSticked = false,
                scrollPointY = 0,
                scrollPointToY = 0,
                scrollDir = 0,
                stickyY = 0,
                elementY = 0,
                elementPreviousY = 0,
                elementRect,
                content,
                contentMatrix;
            function draw() {
                content = content || angular.element(element[0].querySelector(stickySelector));                
                contentMatrix = contentMatrix || new Matrix(content[0]);
                scrollPointY += (scrollPointToY - scrollPointY) / 4;
                if (elementY < scrollPointY) {
                    stickyY = (elementY - scrollPointY) * -1;
                    if (!isSticked) {
                        isSticked = true;
                        element.addClass(stickedClass);
                    }
                } else {
                    stickyY = 0;
                    if (isSticked) {
                        isSticked = false;
                        element.removeClass(stickedClass);
                    }
                }
                contentMatrix.translate.y = -element.height() + stickyY;
                contentMatrix.set();
                return;
            }
            function play() {
                function loop(time) {
                    draw(time);
                    aKey = window.requestAnimationFrame(loop, element);
                }
                if (!aKey) {
                    loop();
                }
            }
            function pause() {
                if (aKey) {
                    window.cancelAnimationFrame(aKey);
                    aKey = null;
                }
            }
            function intersectRect(r1, r2) {
              return !( r2.top > r1.bottom || r2.bottom < r1.top );
            }
            function $onScroll() {
                elementRect = element[0].getBoundingClientRect();
                elementY = elementRect.top;     
                if (elementPreviousY >= elementY) {
                    // console.log('down')                    
                    scrollDir = 0;
                    scrollPointToY = 160;
                } else {
                    // console.log('up')
                    scrollDir = 1;
                    scrollPointToY = 160;
                }
                elementPreviousY = elementY;                
                play();
            }
            function onResize() {
                play();
            }
            var onScroll = $onScroll; // Utils.throttle($onScroll, 1000 / 60);
            function addListeners() {
                angular.element($window).on('resize', onResize);
                angular.element($window).on('scroll', onScroll);
            }
            function removeListeners() {
                angular.element($window).off('resize', onResize);
                angular.element($window).off('scroll', onScroll);
            }
            scope.$on('$destroy', function () {
                removeListeners();
                pause();
            });
            addListeners();
            $onScroll();
        }
    };
}]);

app.directive('resizeableTable', ['$rootScope', '$window', '$timeout', 'Matrix', function ($rootScope, $window, $timeout, Matrix) {
    return {
        restrict: 'A',
        template: '<div style="max-width: 100%;"><div ng-transclude></div></div>',
        transclude: 'true',
        link: function (scope, element, attributes, model) {
            var cols = scope.$eval(attributes.resizeableTable);
            var maxVisibleCols = 0;
            var visibleCols = 0;
            var currentIndex = 0;
            var firstWidth = 144;
            var lastWidth = 30;
            var colWidth = 100;
            scope.isTableColumnVisible = function(index) {
                console.log('resizeableTable.isTableColumnVisible', currentIndex, visibleCols, cols);
                return index >= currentIndex && index - currentIndex < visibleCols;                
            };
            scope.showPrevTableColumn = function() {                
                if (currentIndex > 0) {
                    currentIndex --;
                }
            };
            scope.showNextTableColumn = function() {
                if (currentIndex < cols - visibleCols) {
                    currentIndex ++;
                }
            };
            scope.hasPrevColumns = function() {                
                return currentIndex > 0;
            };
            scope.hasNextColumns = function() {                
                return cols - visibleCols - currentIndex > 0;
            };
            function onResize() {
                $timeout(function() {
                    maxVisibleCols = Math.floor((element[0].offsetWidth - firstWidth - lastWidth) / colWidth);
                    visibleCols = Math.min(cols, maxVisibleCols);
                    currentIndex = Math.max(0, Math.min(cols - visibleCols, currentIndex));
                });
            }
            function addListeners() {
                angular.element($window).on('resize', onResize);
            }
            function removeListeners() {
                angular.element($window).off('resize', onResize);
            }
            scope.$on('$destroy', function () {
                removeListeners();
            });
            addListeners();
            onResize();
        }
    };
}]);

app.controller('MatrixCtrl', ['$scope', '$window', 'VISITS', 'EXPERIENCES', 'FARES', function($scope, $window, VISITS, EXPERIENCES, FARES){

    $scope.visits = VISITS;
    $scope.experiences = EXPERIENCES;
    $scope.fares = FARES;

    $scope.hasVisit = function (experience, visit) {
        var has = false;
        angular.forEach(experience.visits, function (item) {
            if (!has && item === visit.id) {
                has = true;
            };
        });
        return has;
    };

    $scope.isVisible = function (index) {
        
    };

}]);

//# sourceMappingURL=main.js.map
