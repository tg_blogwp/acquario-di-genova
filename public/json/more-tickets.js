[{
    "title": "Dialogue in the dark",
    "abstract": "Path in total absence of light",
    "image": "img/tickets-01.jpg",
    "link": [{
        "template": "adg.partials.shared.buttons.primary",
        "title": "Buy Tickets",
        "uri": "#", 
        "target": "_blank"
    }],
    "subtitle": "Sensorial Itinerary",
    "items": [{
        "title": "Adult",
        "price": "€ 7"
    }, {
        "title": "Children (5-12 years)",
        "price": "€ 4"
    }, {
        "title": "Infant (0-4 years)",
        "price": "Free"
    }],
    "notes": []
}, {
    "title": "Sleep with the Sharks",
    "abstract": "Children&#8217;s best experience",
    "image": "img/tickets-02.jpg",
    "link": [{
        "template": "adg.partials.shared.buttons.primary",
        "title": "Book Now",
        "uri": "#", 
        "target": "_blank"
    }],
    "subtitle": "For Children Only (7-13 years)",
    "items": [{
        "title": "Children (7-13 anni)",
        "price": "€ 80"
    }, {
        "title": "Children (5-12 years)",
        "price": "€ 4"
    }, {
        "title": "Infant (0-4 years)",
        "price": "Free"
    }],
    "notes": [
        "Scheduled once a month maximum of 35 kids"
    ]
}, {
    "title": "Aquarium Pass <br><em>1 year access</em>",
    "abstract": "Follow our activity day by day",
    "image": "img/tickets-03.jpg",
    "link": [{
        "template": "adg.partials.shared.buttons.primary",
        "title": "Buy Pass",
        "uri": "#", 
        "target": "_blank"
    }],
    "subtitle": "Annual Tickets",
    "items": [{
        "title": "Adult",
        "price": "€ 75"
    }, {
        "title": "Children (4-12 years)",
        "price": "€ 40"
    }],
    "notes": []
}]