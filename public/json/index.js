{
    "name": "homepage",
    "hero": "Set sail. Deep dive. Enjoy.",
    "title": "Experiences &amp; Tours",
    "video": "img/acquario-export.mp4",
    "image": null,
    "link": [{
        "template": "adg.partials.shared.buttons.transparent",
        "title": "Experience the Blue Planet",
        "uri": "#", "target": "_self"
    }],
    "items": [{
        "name": "spots",
        "title": "Spots",
        "items": [{
            "title": "Group and <em>School</em> Visits",
            "abstract": "Discover our proposals",
            "image": "img/spot-01.jpg",
            "link": "#"
        }, {
            "title": "Put your mask on and <em>Get -50%</em>",
            "abstract": "Promotional price",
            "image": "img/spot-02.jpg",
            "link": "#"
        }, {
            "title": "Getting around the village",
            "abstract": "A great scenery",
            "image": "img/spot-03.jpg",
            "link": "#"
        }, {
            "title": "Discover the <em>Lovers&#8217, month</em>",
            "abstract": "Couple special price",
            "image": "img/spot-04.jpg",
            "link": "#"
        }],
        "template": "adg.partials.home.spots"
    }, {
        "name": "welcome",
        "title": "Welcome on board",
        "subtitle": "The biggest aquarium in Europe",
        "abstract": "This is Photoshop&#8217;s version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate.",
        "image": "img/background-01.jpg",
        "link": [{
            "template": "adg.partials.shared.buttons.accent",
            "title": "Explore the acquarium",
            "uri": "#", "target": "_self"
        }, {
            "template": "adg.partials.shared.buttons.accent",
            "title": "Discover our location",
            "uri": "#", "target": "_self"
        }],
        "items": [{
            "title": "Animal inside",
            "abstract": "15.000"            
        }, {
            "title": "Sqm Park Area",
            "abstract": "27.000"            
        }, {
            "title": "Visit duration",
            "abstract": "2/3 h"            
        }, {
            "title": "Vegetal species",
            "abstract": "200"            
        }],
        "showbuycta": true,
        "template": "adg.partials.home.welcome"
    }, {
        "name": "shark",
        "title": "The shark bay",
        "subtitle": "Ocean&#8217;s waters",
        "abstract": "<p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo.</p><p>Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi.</p>",
        "image": "img/background-02.jpg",
        "link": [{
            "template": "adg.partials.shared.buttons.secondary",
            "title": "Find out more",
            "uri": "#", "target": "_self"
        }],
        "showbuycta": true,
        "template": "adg.partials.shared.text-left"
    }, {
        "name": "experiences",
        "subtitle": "Discover Our Tours",
        "showbuycta": true,
        "items": null,
        "template": "adg.partials.shared.more-experiences"
    }, {
        "name": "ice-kingdom",
        "title": "The Ice Kingdom",
        "subtitle": "Cold but lovely",
        "abstract": "<p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo.</p><p>Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi.</p>",
        "image": "img/background-03.jpg",
        "link": [{
            "template": "adg.partials.shared.buttons.secondary",
            "title": "Enter the kingdom",
            "uri": "#", "target": "_self"
        }],
        "showbuycta": true,
        "template": "adg.partials.shared.text-right"
    }, {
        "name": "medusa",
        "title": "Medusa&#8217;s dance",
        "subtitle": "No pain just colourful lights",
        "abstract": "<p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo.</p>",
        "image": "img/background-04.jpg",
        "link": [{
            "template": "adg.partials.shared.buttons.secondary",
            "title": "Enchant yourself",
            "uri": "#", "target": "_self"
        }],
        "showbuycta": true,
        "template": "adg.partials.shared.text-center"
    }, {
        "name": "follow-us",
        "title": "Follow Us",
        "showbuycta": false,
        "items": null,
        "template": "adg.partials.home.social-wall"
    }, {
        "name": "dolphins",
        "title": "Cetacean &amp; dolphin&#8217;s pavillion",
        "subtitle": "The funniest show",
        "abstract": "<p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo.</p>",
        "image": "img/background-05.jpg",
        "link": [{
            "template": "adg.partials.shared.buttons.secondary",
            "title": "Find out more",
            "uri": "#", "target": "_self"
        }],
        "news": {
            "name": "goccia",
            "title": "Goccia: The dolphin calf born last September",
            "image": "img/news.jpg",
            "link": [{
                "template": "adg.partials.shared.buttons.accent",
                "title": "Read more",
                "uri": "#", "target": "_self"
            }]
        },
        "showbuycta": true,
        "template": "adg.partials.shared.text-center-closure"
    }, {
        "name": "visits",
        "subtitle": "Not only the Acquarium",
        "showbuycta": true,
        "items": null,
        "template": "adg.partials.shared.more-visits"
    }, {
        "name": "closure",
        "title": "Acquario di Genova.",
        "subtitle": "Set sail and deep dive into an ocean of surprises.",
        "image": "img/background-06.jpg",
        "template": "adg.partials.home.closure"
    }],
    "template": "adg.index"
}
