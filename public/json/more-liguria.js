[{
    "title": "Genova art and culture",
    "abstract": "2 nights/3 days",
    "image": "img/liguria-01.jpg",
    "link": [{
        "template": "adg.partials.shared.buttons.secondary",
        "title": "Discover More",
        "uri": "#", 
        "target": "_blank"
    }],
    "subtitle": "All year round, week-ends",
    "bodycol1": "3 days in the city with the biggest historic centre in Europe, a net of small and narrow streets rich in old workshops, palaces and typical frying shops!",
    "items": [],
    "notes": []
}, {
    "title": "Magical villages art, nature and gastronomy",
    "abstract": "2 nights/3 days",
    "image": "img/liguria-02.jpg",
    "link": [{
        "template": "adg.partials.shared.buttons.secondary",
        "title": "Discover More",
        "uri": "#", 
        "target": "_blank"
    }],
    "subtitle": "From March to September: min. 2 people",
    "bodycol1": "3 days tour to discover the Ligurian east coast, a wonderful heaven able to agree smart set lovers, romantics and hiking and diving fans.",
    "items": [],
    "notes": []
}, {
    "title": "Liguria Tour: highlights!",
    "abstract": "7 nights/8 days",
    "image": "img/liguria-03.jpg",
    "link": [{
        "template": "adg.partials.shared.buttons.secondary",
        "title": "Discover More",
        "uri": "#", 
        "target": "_blank"
    }],
    "subtitle": "From April to October",
    "bodycol1": "The best ligurian highlights in a 8 day/7 night: Art, History, Music in Genoa; tastings in Cinque Terre and Portofino; Relax, Sea, Wellness in the Western Riviera, and Sport in Alassio.",
    "items": [],
    "notes": []
}]