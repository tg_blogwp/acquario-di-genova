[{
	"id": 1,
    "name": "genova-aquarium",
    "title": "Genova Aquarium",
    "hero": "Set Sail. Deep Dive. Enjoy.",
    "abstract": "Acquario di Genova is the largest aquarium in Italy, boasting the richest exposition of water biodiversity in Europe. You can dive into over seventy eco-systems.",
    "bodycol1": "",
    "video": null,
    "image": "img/more-01.jpg"
}, {
	"id": 2,
    "name": "secret-aquarium",
    "title": "Secret Aquarium",
    "hero": "Secret Pools. Behing the Scenes.",
    "abstract": "An exhibition with sensorial itinerary, a &quot;journey&quot; in the dark that transforms familiar places and gestures into an extraordinary experience.",
    "bodycol1": "",
    "video": null,
    "image": "img/more-04.jpg"
}, {
	"id": 3,
    "name": "biosphere",
    "title": "Biosphere",
    "hero": "Tropical. Forest. Reptiles.",
    "abstract": "A \"drop\" of steel and glass suspended over the sea, which hosts vegetable and animal species coming from tropical forests threatened by human exploitation. You will be able to view them from close, admire their fragile beauty and understand how their survival also depends on you.",
    "bodycol1": "",
    "video": null,
    "image": "img/more-06.jpg"
}, {
	"id": 4,
    "name": "galata-museum",
    "title": "Galata Museum",
    "hero": "Navigation. History. A Full Immersion.",
    "abstract": "The largest and most innovative sea museum in the Mediterranean: a reconstruction of a 16th century, 33-metre long Genoa galley and an 18th century brigantine-schooner, six thousand original objects in the twenty three display cases of Galata for a full immersion in navigation history.",
    "bodycol1": "",
    "video": null,
    "image": "img/more-04.jpg"
}, {
	"id": 5,
    "name": "submarine-ns518",
    "title": "Submarine NS518",
    "hero": "Nazario Sauro. Submarine Museum.",
    "abstract": "Stepping on board the Nazario Sauro, the first submarine to be turned into a museum on water, and visiting the dedicated pre-show inside the Galata Museo del Mare is an important enrichment of the museum path, of the \"Darsena\", of the surrounding area and of the Acquario Village world.",
    "bodycol1": "",
    "video": null,
    "image": "img/more-06.jpg"
}, {
	"id": 6,
    "name": "antartic-museum",
    "title": "Antartic Museum",
    "hero": "Learn. Charm. Antartic.",
    "abstract": "To learn about an unknown and charming land. At the discovery of the Italian base in Antarctica and the most suggestive scenarios through films, reconstructions and scientific finding. Interactive and advanced displays will allow you to discover the last uncontaminated continent and to follow the activity of Italian researches in Antarctica, the vast natural laboratory to study environmental and climatic problems.",
    "bodycol1": "",
    "video": null,
    "image": "img/more-02.jpg"
}, {
	"id": 7,
    "name": "bigo-panoramic",
    "title": "Bigo Panoramic",
    "hero": "Big. Panoramic. Amusement.",
    "abstract": "A panoramic lift offering a 360° view on Porto Antico di Genova. Bigo was designed by architect Renzo Piano to offer you an amazing aerial view of one of the largest historical centres of Europe, rich of art.",
    "bodycol1": "",
    "video": null,
    "image": "img/more-03.jpg"
}]
