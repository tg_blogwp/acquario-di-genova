{
    "name": "acquario-village",
    "label": "Acquario Village",
    "hero": "Learn. Amuse. Experience.",
    "title": "Acquario Village",
    "subtitle": "A multiplier of positive energies.",
    "abstract": "",
    "bodycol1": "<p>You get an engine of discoveries to live in the structures of AcquarioVillage’s world.</p><p>The AcquarioVillage pass allows you to visit all the Costa Edutainment facilities of the Porto Antico at a special price.</p>",
    "bodycol2": "<p>The AcquarioVillage ticket is valid for a year from the date of purchase and it is up to you to use it in one day or over more days in the course of the year.</p><p>Get the maximum flexibility for the utmost convenience.</p>",
    "video": null,
    "image": "img/acquario-village-01.jpg",
    "link": null,
    "showbuycta": false,
    "items": [{
        "name": "acquarium",
        "label": "Acquarium",
        "title": "Acquarium and Tropical Garden",
        "subtitle": "Visit the largest aquarium in Italy.",
        "description": "Acquarium and Tropical Garden",
        "abstract": "<p>Boasting the richest exposition of water biodiversity in Europe. You can dive into over seventy eco-systems.</p>",
        "image": "img/acquario-village-02.jpg",
        "link": [{
            "template": "adg.partials.shared.buttons.secondary",
            "title": "Find out more",
            "uri": "#",
            "target": "_self"
        }],
        "showbuycta": true,
        "template": "adg.partials.shared.text-left"
    }, {
        "name": "galata-museum",
        "label": "Galata Museum",
        "title": "Galata Museum",
        "subtitle": "Dive into the Sea Museum",
        "description": "Galata Museum",
        "abstract": "<p>The largest and most innovative sea museum in the Mediterranean.</p>",
        "image": "img/acquario-village-03.jpg",
        "link": [{
            "template": "adg.partials.shared.buttons.secondary",
            "title": "Find out more",
            "uri": "#", "target": "_self"
        }],
        "showbuycta": true,
        "template": "adg.partials.shared.text-right"
    }, {
        "name": "submarine-s518",
        "label": "Nazario Sauro Submarine",
        "title": "Submarine S 518 Nazario Sauro",
        "subtitle": "A Museum Ship in Genova.",
        "description": "Submarine S 518 Nazario Sauro",
        "abstract": "<p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo.</p>",
        "image": "img/acquario-village-04.jpg",
        "link": [{
            "template": "adg.partials.shared.buttons.secondary",
            "title": "Find out more",
            "uri": "#", "target": "_self"
        }],
        "showbuycta": true,
        "template": "adg.partials.shared.text-center"
    }, {
        "name": "national-museum-of-antarctica",
        "label": "National Museum of Antarctica",
        "title": "National Museum of Antarctica",
        "subtitle": "An unknown and charming land",
        "description": "National Museum of Antarctica",
        "abstract": "<p>At the discovery of the most suggestive scenarios through films, reconstructions and scientific finding.</p>",
        "image": "img/acquario-village-05.jpg",
        "link": [{
            "template": "adg.partials.shared.buttons.secondary",
            "title": "Find out more",
            "uri": "#", "target": "_self"
        }],
        "showbuycta": true,
        "template": "adg.partials.shared.text-left"
    }, {
        "name": "biosphere",
        "label": "Biosphere",
        "title": "Biosphere",
        "subtitle": "An ecosystem of million years ago",
        "description": "Biosphere",
        "abstract": "<p>A &#8220;drop&#8221; of steel and glass suspended over the sea, which hosts vegetable and animal species coming from tropical forests threatened by human exploitation.</p>",
        "image": "img/acquario-village-06.jpg",
        "link": [{
            "template": "adg.partials.shared.buttons.secondary",
            "title": "Find out more",
            "uri": "#", "target": "_self"
        }],
        "showbuycta": true,
        "template": "adg.partials.shared.text-right"
    }, {
        "name": "bigo",
        "label": "Bigo Panoramic Lift",
        "title": "Bigo Panoramic Lift",
        "subtitle": "An amazing aerial view of Genova",
        "description": "Bigo Panoramic Lift",
        "abstract": "<p>A panoramic lift offering a 360° view on Porto Antico di Genova. Bigo was designed by architect Renzo Piano to offer you an amazing aerial view of one of the largest historical centres of Europe, rich of art.</p>",
        "image": "img/acquario-village-07.jpg",
        "link": [{
            "template": "adg.partials.shared.buttons.secondary",
            "title": "Find out more",
            "uri": "#", "target": "_self"
        }],
        "news": null,
        "showbuycta": true,
        "template": "adg.partials.shared.text-center-closure"
    }, {
        "name": "plan",
        "title": "Timetables &amp; Prices",
        "subtitle": "Plan Your Visit",
        "abstract": "",
        "image": null,
        "showbuycta": false,
        "items": null,
        "template": "adg.partials.shared.plan-visit"
    }, {
        "name": "visits",
        "title": "",
        "subtitle": "Not only the Acquario Village",
        "abstract": "",
        "showbuycta": true,
        "items": null,
        "template": "adg.partials.shared.more-experiences"
    }]
}
