{
    "name": "info",
    "label": "Info",
    "title": "Info",
    "subtitle": "Prices. Timetables. Getting here.",
    "bodycol1": "<p>Discover all the information you need for an unforgettable experience at Acquario di Genova.</p>",
    "bodycol2": "<p>Opening today from 8am to 9pm<br>Last admittance 8pm</p><p>Acquario di Genova<br>Ponte Spinola, 16128 Genova, Italia</p>",
    "video": null,
    "image": null,
    "link": null,
    "items": [{
        "name": "prices",
        "label": "Prices",
        "subtitle": "Prices",
        "bodycol1": "<p>To fulfill all of your needs many different Tours &amp; Experience packs are availables in Acquario di Genova.</p><p>For price details of every pack let&#8217;s start planning your visit in the ticket page.</p>",
        "bodycol2": "<p>All passes are valid for a year from the date of purchase and it is up to you to use it in one day or over more days in the course of the year.</p>",
        "image": null,
        "link": {
            "template": "adg.partials.shared.buttons.primary",
            "title": "PLAN VISIT &amp; BUY TICKETS",
            "uri": "#", "target": "_self"
        },
        "showbuycta": false,
        "items": null,
        "template": "adg.partials.shared.three-cols"
    }, {
        "name": "durations",
        "label": "Durations",
        "subtitle": "Durations",
        "bodycol1": "",
        "bodycol2": "",
        "image": null,
        "link": null,
        "showbuycta": false,
        "items": null,
        "template": "adg.partials.shared.durations"
    }, {
        "name": "timetables",
        "label": "Timetables",
        "subtitle": "Opening Hours",
        "bodycol1": "",
        "bodycol2": "",
        "image": null,
        "link": null,
        "showbuycta": false,
        "items": null,
        "template": "adg.partials.shared.timetables"
    }, {
        "name": "getting-here",
        "label": "Getting here",
        "subtitle": "Getting here",
        "bodycol1": "<p>The Acquario of Genova is located in the beautiful Ponte Spinola area, just steps away from Genova&#8217;s Old Port and minutes from downtown Genova.</p>",
        "bodycol2": "",
        "template": "adg.partials.shared.three-cols",
        "image": null,
        "link": null,
        "showbuycta": false,
        "items": [{
            "name": "by-car",
            "label": "By Car",
            "subtitle": "By Car",
            "bodycol1": "<p>Take the Genova-Ovest exit and proceed towards the centre for around 2km. Direction Via Milano and Via Gramsci. You will find the car park 50 meters before the Piazza Caricamento underpass on your right.</p>",
            "bodycol2": "",
            "image": null,
            "link": null,
            "showbuycta": false,
            "template": "adg.partials.shared.subs.two-cols"
        }, {
            "name": "by-bus",
            "label": "By Bus",
            "subtitle": "By Bus",
            "bodycol1": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque efficitur massa magna, ut viverra lectus condimentum eget. Curabitur mi sem, scelerisque a tortor egestas, aliquet posuere ligula. Vivamus nec lectus justo. Integer egestas ullamcorper gravida. Donec nec blandit eros. Sed nisi felis, sagittis ac vestibulum vitae, viverra sed augue. Sed accumsan accumsan enim, a fringilla dui efficitur quis. Nam suscipit lorem sem, sit amet vulputate lacus interdum vitae.</p>",
            "bodycol2": "",
            "image": null,
            "link": null,
            "showbuycta": false,
            "template": "adg.partials.shared.subs.two-cols"
        }, {
            "name": "by-train",
            "label": "By Train",
            "subtitle": "By Train",
            "bodycol1": "<p>Etiam sed efficitur metus, non molestie orci. Etiam dapibus orci et mauris porttitor, quis facilisis augue ullamcorper. Curabitur maximus est in arcu convallis, vel maximus velit laoreet. Nullam et urna a magna varius ultricies. Integer viverra nisl in ipsum consectetur, eget feugiat justo ullamcorper. Vivamus fringilla eros eu risus maximus euismod.</p>",
            "bodycol2": "",
            "image": null,
            "link": null,
            "showbuycta": false,
            "template": "adg.partials.shared.subs.two-cols"
        }, {
            "name": "by-bike",
            "label": "By Bike",
            "subtitle": "By Bike",
            "bodycol1": "<p>Duis maximus erat sed sem malesuada porttitor. Sed elit magna, pulvinar ac turpis non, mollis ullamcorper tortor. Integer et placerat lorem. Quisque a vulputate velit. In sit amet magna malesuada, volutpat nulla et, scelerisque urna. Nulla rutrum posuere fringilla. Proin sodales mauris congue urna malesuada porta.</p>",
            "bodycol2": "",
            "image": null,
            "link": null,
            "showbuycta": false,
            "template": "adg.partials.shared.subs.two-cols"
        }]
    }, {
        "name": "parking",
        "label": "Parking",
        "subtitle": "Parking",
        "bodycol1": "<p>The Aquarium&#8217;s car park is open 24 hrs a day, every day.</p><p>It has space for a total of 167 vehicles and 4 for disabled badge holders.</p>",
        "bodycol2": "",
        "template": "adg.partials.shared.three-cols",
        "image": null,
        "link": null,
        "showbuycta": false,
        "items": [{
            "name": "car-fares",
            "label": "Car Fares",
            "subtitle": "Car Fares",
            "bodycol1": "<table class=\"table\"><tbody><tr><td class=\"important\">&euro;2,00</td><td>per hour</td><td>from 7:00 am to 12:00 am</td></tr><tr><td class=\"important\">&euro;1,00</td><td>per hour</td><td>from 12:00 am to 7:00 am</td></tr></tbody></table>",
            "bodycol2": "<p>On request, you can buy a parking card to be used on nights from 7pm to 8am, valid 30 days. The parking card costs 50,00 €.</p><p>It is not possible to reserve a parking space. In the Porto Antico Area there are several parking areas.</p><p><a href=\"#\" class=\"a-link\">Click here for more information.</a></p>",
            "image": null,
            "link": null,
            "showbuycta": false,
            "template": "adg.partials.shared.subs.two-cols"
        }, {
            "name": "camper-fares",
            "label": "Camper Fares",
            "subtitle": "Camper Fares",
            "bodycol1": "<table class=\"table\"><tbody><tr><td class=\"important\">&euro;2,00</td><td>per hour</td><td>from 7:00 am to 12:00 am</td></tr><tr><td class=\"important\">&euro;1,00</td><td>per hour</td><td>from 12:00 am to 7:00 am</td></tr></tbody></table>",
            "bodycol2": "<p>On request, you can buy a parking card to be used on nights from 7pm to 8am, valid 30 days. The parking card costs 50,00 €.</p><p>It is not possible to reserve a parking space. In the Porto Antico Area there are several parking areas.</p><p><a href=\"#\" class=\"a-link\">Click here for more information.</a></p>",
            "image": null,
            "link": null,
            "showbuycta": false,
            "template": "adg.partials.shared.subs.two-cols"
        }]
    }, {
        "name": "services",
        "label": "Services",
        "subtitle": "Services",
        "bodycol1": "<p>Acquario di Genova is a structure that is continually evolving. The facilites are excellent and catering to the needs of all our visitors is a top priority.</p>",
        "bodycol2": "<p>For this reason, a range of services and facilites have been made available along the exhibition routes that we hope will enable visitors to enjoy and make the most of their time with us.</p>",
        "template": "adg.partials.shared.three-cols",
        "image": null,
        "link": null,
        "showbuycta": false,
        "items": [{
            "name": "internal-facilities",
            "label": "Internal facilities",
            "subtitle": "Internal facilities",
            "bodycol1": "<p><b>Cloakroom.</b> There is a staffed cloakroom where visitors can deposit items of clothing &euro; 2,00 per item and luggage € 2,00 per piece. Here, child backpack carriers are also available, free of charge, for use during your visit.</p><p><b>Guide book.</b> Available in Italian, English, French, German and Spanish, can be purchased at reception for &euro; 3,50.</p><p><b>Multimedia guide.</b> Available in Italian and English on tablets to be rent at the Reception Desk for &euro; 5,00.</p><p><b>Souvenir photoes.</b> You can have a souvenir photo taken with Splaffy, the Aquarium’s mascot, at the beginning of your visit.</p><p><b>Gift shops.</b> There are two gift shops availables for you. One midway and one at the end of the exhibition route where you can buy original items available exclusively at the Aquarium.</p><p><b>Snack bar.</b> Our Snack bar provides the opportunity to taste a local speciality, the famous “focaccia genovese”.</p>",
            "bodycol2": "<p><b>Also on site</b></p><p>Toilets with baby-changing facilities.</p><p>Lift for disabled visitors.</p><p>Rest areas with plenty of hot and cold drinks machines.</p>",
            "image": null,
            "link": null,
            "showbuycta": false,
            "template": "adg.partials.shared.subs.two-cols"
        }, {
            "name": "external-facilities",
            "label": "External Facilities",
            "subtitle": "External Facilities",
            "bodycol1": "<p><b>Cloakroom.</b> There is a staffed cloakroom where visitors can deposit items of clothing &euro; 2,00 per item and luggage € 2,00 per piece. Here, child backpack carriers are also available, free of charge, for use during your visit.</p><p><b>Guide book.</b> Available in Italian, English, French, German and Spanish, can be purchased at reception for &euro; 3,50.</p><p><b>Multimedia guide.</b> Available in Italian and English on tablets to be rent at the Reception Desk for &euro; 5,00.</p><p><b>Souvenir photoes.</b> You can have a souvenir photo taken with Splaffy, the Aquarium’s mascot, at the beginning of your visit.</p><p><b>Gift shops.</b> There are two gift shops availables for you. One midway and one at the end of the exhibition route where you can buy original items available exclusively at the Aquarium.</p><p><b>Snack bar.</b> Our Snack bar provides the opportunity to taste a local speciality, the famous “focaccia genovese”.</p>",
            "bodycol2": "<p><b>Also on site</b></p><p>Toilets with baby-changing facilities.</p><p>Lift for disabled visitors.</p><p>Rest areas with plenty of hot and cold drinks machines.</p>",
            "image": null,
            "link": null,
            "showbuycta": false,
            "template": "adg.partials.shared.subs.two-cols"
        }, {
            "name": "restrictions",
            "label": "Restrictions",
            "subtitle": "Restrictions",
            "bodycol1": "<p><b>Cloakroom.</b> There is a staffed cloakroom where visitors can deposit items of clothing &euro; 2,00 per item and luggage € 2,00 per piece. Here, child backpack carriers are also available, free of charge, for use during your visit.</p><p><b>Guide book.</b> Available in Italian, English, French, German and Spanish, can be purchased at reception for &euro; 3,50.</p><p><b>Multimedia guide.</b> Available in Italian and English on tablets to be rent at the Reception Desk for &euro; 5,00.</p><p><b>Souvenir photoes.</b> You can have a souvenir photo taken with Splaffy, the Aquarium’s mascot, at the beginning of your visit.</p><p><b>Gift shops.</b> There are two gift shops availables for you. One midway and one at the end of the exhibition route where you can buy original items available exclusively at the Aquarium.</p><p><b>Snack bar.</b> Our Snack bar provides the opportunity to taste a local speciality, the famous “focaccia genovese”.</p>",
            "bodycol2": "<p><b>Also on site</b></p><p>Toilets with baby-changing facilities.</p><p>Lift for disabled visitors.</p><p>Rest areas with plenty of hot and cold drinks machines.</p>",
            "image": null,
            "link": null,
            "showbuycta": false,
            "template": "adg.partials.shared.subs.two-cols"
        }, {
            "name": "facilities-for-the-disabled",
            "label": "Facilities for the disabled",
            "subtitle": "Facilities for the disabled",
            "bodycol1": "<p><b>Cloakroom.</b> There is a staffed cloakroom where visitors can deposit items of clothing &euro; 2,00 per item and luggage € 2,00 per piece. Here, child backpack carriers are also available, free of charge, for use during your visit.</p><p><b>Guide book.</b> Available in Italian, English, French, German and Spanish, can be purchased at reception for &euro; 3,50.</p><p><b>Multimedia guide.</b> Available in Italian and English on tablets to be rent at the Reception Desk for &euro; 5,00.</p><p><b>Souvenir photoes.</b> You can have a souvenir photo taken with Splaffy, the Aquarium’s mascot, at the beginning of your visit.</p><p><b>Gift shops.</b> There are two gift shops availables for you. One midway and one at the end of the exhibition route where you can buy original items available exclusively at the Aquarium.</p><p><b>Snack bar.</b> Our Snack bar provides the opportunity to taste a local speciality, the famous “focaccia genovese”.</p>",
            "bodycol2": "<p><b>Also on site</b></p><p>Toilets with baby-changing facilities.</p><p>Lift for disabled visitors.</p><p>Rest areas with plenty of hot and cold drinks machines.</p>",
            "image": null,
            "link": null,
            "showbuycta": false,
            "template": "adg.partials.shared.subs.two-cols"
        }, {
            "name": "parking",
            "label": "Parking",
            "subtitle": "Parking",
            "bodycol1": "<p><b>Cloakroom.</b> There is a staffed cloakroom where visitors can deposit items of clothing &euro; 2,00 per item and luggage € 2,00 per piece. Here, child backpack carriers are also available, free of charge, for use during your visit.</p><p><b>Guide book.</b> Available in Italian, English, French, German and Spanish, can be purchased at reception for &euro; 3,50.</p><p><b>Multimedia guide.</b> Available in Italian and English on tablets to be rent at the Reception Desk for &euro; 5,00.</p><p><b>Souvenir photoes.</b> You can have a souvenir photo taken with Splaffy, the Aquarium’s mascot, at the beginning of your visit.</p><p><b>Gift shops.</b> There are two gift shops availables for you. One midway and one at the end of the exhibition route where you can buy original items available exclusively at the Aquarium.</p><p><b>Snack bar.</b> Our Snack bar provides the opportunity to taste a local speciality, the famous “focaccia genovese”.</p>",
            "bodycol2": "<p><b>Also on site</b></p><p>Toilets with baby-changing facilities.</p><p>Lift for disabled visitors.</p><p>Rest areas with plenty of hot and cold drinks machines.</p>",
            "image": null,
            "link": null,
            "showbuycta": false,
            "template": "adg.partials.shared.subs.two-cols"
        }, {
            "name": "entrance-tickets",
            "label": "Entrance Tickets",
            "subtitle": "Entrance Tickets",
            "bodycol1": "<p><b>Cloakroom.</b> There is a staffed cloakroom where visitors can deposit items of clothing &euro; 2,00 per item and luggage € 2,00 per piece. Here, child backpack carriers are also available, free of charge, for use during your visit.</p><p><b>Guide book.</b> Available in Italian, English, French, German and Spanish, can be purchased at reception for &euro; 3,50.</p><p><b>Multimedia guide.</b> Available in Italian and English on tablets to be rent at the Reception Desk for &euro; 5,00.</p><p><b>Souvenir photoes.</b> You can have a souvenir photo taken with Splaffy, the Aquarium’s mascot, at the beginning of your visit.</p><p><b>Gift shops.</b> There are two gift shops availables for you. One midway and one at the end of the exhibition route where you can buy original items available exclusively at the Aquarium.</p><p><b>Snack bar.</b> Our Snack bar provides the opportunity to taste a local speciality, the famous “focaccia genovese”.</p>",
            "bodycol2": "<p><b>Also on site</b></p><p>Toilets with baby-changing facilities.</p><p>Lift for disabled visitors.</p><p>Rest areas with plenty of hot and cold drinks machines.</p>",
            "image": null,
            "link": null,
            "showbuycta": false,
            "template": "adg.partials.shared.subs.two-cols"
        }, {
            "name": "dog-sitting",
            "label": "Dog sitting",
            "subtitle": "Dog sitting",
            "bodycol1": "<p><b>Cloakroom.</b> There is a staffed cloakroom where visitors can deposit items of clothing &euro; 2,00 per item and luggage € 2,00 per piece. Here, child backpack carriers are also available, free of charge, for use during your visit.</p><p><b>Guide book.</b> Available in Italian, English, French, German and Spanish, can be purchased at reception for &euro; 3,50.</p><p><b>Multimedia guide.</b> Available in Italian and English on tablets to be rent at the Reception Desk for &euro; 5,00.</p><p><b>Souvenir photoes.</b> You can have a souvenir photo taken with Splaffy, the Aquarium’s mascot, at the beginning of your visit.</p><p><b>Gift shops.</b> There are two gift shops availables for you. One midway and one at the end of the exhibition route where you can buy original items available exclusively at the Aquarium.</p><p><b>Snack bar.</b> Our Snack bar provides the opportunity to taste a local speciality, the famous “focaccia genovese”.</p>",
            "bodycol2": "<p><b>Also on site</b></p><p>Toilets with baby-changing facilities.</p><p>Lift for disabled visitors.</p><p>Rest areas with plenty of hot and cold drinks machines.</p>",
            "image": null,
            "link": null,
            "showbuycta": false,
            "template": "adg.partials.shared.subs.two-cols"
        }, {
            "name": "service-charter",
            "label": "Service Charter",
            "subtitle": "Service Charter",
            "bodycol1": "<p><b>Cloakroom.</b> There is a staffed cloakroom where visitors can deposit items of clothing &euro; 2,00 per item and luggage € 2,00 per piece. Here, child backpack carriers are also available, free of charge, for use during your visit.</p><p><b>Guide book.</b> Available in Italian, English, French, German and Spanish, can be purchased at reception for &euro; 3,50.</p><p><b>Multimedia guide.</b> Available in Italian and English on tablets to be rent at the Reception Desk for &euro; 5,00.</p><p><b>Souvenir photoes.</b> You can have a souvenir photo taken with Splaffy, the Aquarium’s mascot, at the beginning of your visit.</p><p><b>Gift shops.</b> There are two gift shops availables for you. One midway and one at the end of the exhibition route where you can buy original items available exclusively at the Aquarium.</p><p><b>Snack bar.</b> Our Snack bar provides the opportunity to taste a local speciality, the famous “focaccia genovese”.</p>",
            "bodycol2": "<p><b>Also on site</b></p><p>Toilets with baby-changing facilities.</p><p>Lift for disabled visitors.</p><p>Rest areas with plenty of hot and cold drinks machines.</p>",
            "image": null,
            "link": null,
            "showbuycta": false,
            "template": "adg.partials.shared.subs.two-cols"
        }]
    }, {
        "name": "faq",
        "label": "Faq",
        "subtitle": "Faq",
        "bodycol2": "<p></p>",
        "bodycol1": "<p></p>",
        "template": "adg.partials.shared.faq",
        "image": null,
        "link": null,
        "showbuycta": false,
        "items": [{
            "name": "faq-01",
            "title": "How much does it cost the visit to Acquario?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-02",
            "title": "Which are the opening hours?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-03",
            "title": "Which services are available within Acquario?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-04",
            "title": "What can't I do?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-05",
            "title": "May I take pictures?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-06",
            "title": "May I use my videocamera?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-07",
            "title": "Where is the nearest parking?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-08",
            "title": "May I book my parking?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-09",
            "title": "May I park motos in Acquario parking?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-10",
            "title": "Are there any shows in Acquario?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-11",
            "title": "Which are the meal hours for sharks, dolphins, seals and penguins?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-12",
            "title": "May I go out of Acquario and then get in back?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-13",
            "title": "May I touch the animals or dive in the pools?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-14",
            "title": "Is the structure reachable for disabled people?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-15",
            "title": "May I take pets in Acquario?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-16",
            "title": "How can I get to Acquario?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-17",
            "title": "Is there a path to follow in the Aquarium?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-18",
            "title": "Are there any guided tours?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-19",
            "title": "How much time does the visit take?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-20",
            "title": "How long can I stay in Acquario?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-21",
            "title": "Does the entrance ticket include the entrance to Tropical Garden - Wings Flapping?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-22",
            "title": "Where can I buy the ticket for the Tropical Garden - Wings Flapping?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-23",
            "title": "Is there a restaurant within the Acquario?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-24",
            "title": "Which is the nearest restaurant?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-25",
            "title": "Where can I buy the tickets for Acquario di Genova?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }, {
            "name": "faq-26",
            "title": "Are there times recommended for visiting the Aquarium of Genoa?",
            "abstract": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies libero mauris, eget ullamcorper libero hendrerit eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>"
        }]
    }, {
        "name": "contacts",
        "label": "Contacts",
        "subtitle": "Contacts",
        "bodycol1": "<p><b>Costa Edutainment S.p.A.<br>Acquario di Genova</b></p><p>Area Porto Antico, Ponte Spinola<br>16128 Genova</p>",
        "bodycol2": "<p><a href=\"#\" class=\"btn btn-tel btn-block\">Tel +39 010 234 51</a><a href=\"#\" class=\"btn btn-tel btn-block\">Fax +39 010 256 160</a></p>",
        "template": "adg.partials.shared.three-cols",
        "image": null,
        "link": null,
        "showbuycta": false,
        "items": [{
            "name": "information",
            "label": "Information",
            "subtitle": "Information",
            "bodycol1": "<p>If you wish to ask any questions to Acquario di Genova, please fill the following form.</p>",
            "bodycol2": "<p><b>Call us by phone</b><br><br><a href=\"#\" class=\"btn btn-tel btn-block\">Tel +39 010 234 5678</a></p>",
            "image": null,
            "link": null,
            "showbuycta": false,
            "template": "adg.partials.shared.subs.two-cols"       
        }, {
            "name": "commercial-office",
            "label": "Commercial Office",
            "subtitle": "Commercial Office",
            "bodycol1": "<p>If you wish to ask any questions to Acquario di Genova, please fill the following form.</p>",
            "bodycol2": "<p><b>Call us by phone</b><br><br><a href=\"#\" class=\"btn btn-tel btn-block\">Tel +39 010 234 5678</a></p>",
            "image": null,
            "link": null,
            "showbuycta": false,
            "template": "adg.partials.shared.subs.two-cols"       
        }, {
            "name": "office-events",
            "label": "Office Events",
            "subtitle": "Office Events",
            "bodycol1": "<p>If you wish to ask any questions to Acquario di Genova, please fill the following form.</p>",
            "bodycol2": "<p><b>Call us by phone</b><br><br><a href=\"#\" class=\"btn btn-tel btn-block\">Tel +39 010 234 5678</a></p>",
            "image": null,
            "link": null,
            "showbuycta": false,
            "template": "adg.partials.shared.subs.two-cols"       
        }, {
            "name": "office-marketing-and-ommunication",
            "label": "Office Marketing &amp; Communication",
            "subtitle": "Office Marketing &amp; Communication",
            "bodycol1": "<p>If you wish to ask any questions to Acquario di Genova, please fill the following form.</p>",
            "bodycol2": "<p><b>Call us by phone</b><br><br><a href=\"#\" class=\"btn btn-tel btn-block\">Tel +39 010 234 5678</a></p>",
            "image": null,
            "link": null,
            "showbuycta": false,
            "template": "adg.partials.shared.subs.two-cols"       
        }, {
            "name": "incoming-liguria",
            "label": "Incoming Liguria",
            "subtitle": "Incoming Liguria",
            "bodycol1": "<p>If you wish to ask any questions to Acquario di Genova, please fill the following form.</p>",
            "bodycol2": "<p><b>Call us by phone</b><br><br><a href=\"#\" class=\"btn btn-tel btn-block\">Tel +39 010 234 5678</a></p>",
            "image": null,
            "link": null,
            "showbuycta": false,
            "template": "adg.partials.shared.subs.two-cols"            
        }, {
            "name": "schools-and-education",
            "label": "Schools & Education",
            "subtitle": "Schools & Education",
            "bodycol1": "<p>If you wish to ask any questions to Acquario di Genova, please fill the following form.</p>",
            "bodycol2": "<p><b>Call us by phone</b><br><br><a href=\"#\" class=\"btn btn-tel btn-block\">Tel +39 010 234 5678</a></p>",
            "image": null,
            "link": null,
            "showbuycta": false,
            "template": "adg.partials.shared.subs.two-cols"            
        }, {
            "name": "jobs",
            "label": "Jobs",
            "subtitle": "Jobs",
            "bodycol1": "<p>If you wish to ask any questions to Acquario di Genova, please fill the following form.</p>",
            "bodycol2": "<p><b>Call us by phone</b><br><br><a href=\"#\" class=\"btn btn-tel btn-block\">Tel +39 010 234 5678</a></p>",
            "image": null,
            "link": null,
            "showbuycta": false,
            "template": "adg.partials.shared.subs.two-cols"            
        }]
    }],
    "template": "adg.info"
}
