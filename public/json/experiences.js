[{
    "id": 1,
    "name": "genova-aquarium",
    "title": "Aquarium Only",
    "hero": "Richest Exposition. Seventy Eco-Systems.",
    "abstract": "Acquario di Genova is the largest aquarium in Italy, boasting the richest exposition of water biodiversity in Europe. You can dive into over seventy eco-systems.",
    "bodycol1": "",
    "video": null,
    "images": ["img/tweet-02.jpg", "img/polaroid-01.jpg"],    
    "visits": [1],
    "duration": "3 h",
    "fares": [{
        "id": 1,
        "title": "Free"
    }, {
        "id": 1,
        "title": "€ 15"
    }, {
        "id": 1,
        "title": "€ 24"
    }]
}, {
    "id": 2,
    "name": "secret-aquarium",
    "title": "Secret Aquarium",
    "hero": "Secret Pools. Behing the Scenes.",
    "abstract": "Go back stage and expose the secrets of the Aquarium&#8217;s pools and tanks. Make the most of this opportunity to go behind the scenes and see what is going on. And, once the 90 minute guided tour is over, you can continue to explore the public exhibits on your own.",
    "bodycol1": "",
    "video": null,
    "images": ["img/tweet-02.jpg", "img/polaroid-02.jpg"],
    "visits": [2],
    "duration": "2 h",
    "fares": [{
        "id": 1,
        "title": "Free"
    }, {
        "id": 1,
        "title": "€ 18"
    }, {
        "id": 1,
        "title": "€ 29"
    }]
}, {
    "id": 3,
    "name": "village-pack",
    "title": "Village Pack",
    "hero": "Set Sail. Deep Dive. Enjoy.",
    "abstract": "AcquarioVillage is the place in which learning becomes an amusing and enthralling experience. A multiplier of positive energies, an engine of discoveries to live in the structures of AcquarioVillage&#8217;s world.",
    "bodycol1": "",
    "video": null,
    "images": ["img/tweet-02.jpg", "img/polaroid-03.jpg"],
    "visits": [1,3,4,5,6,7],
    "duration": "6 h",
    "fares": [{
        "id": 1,
        "title": "Free"
    }, {
        "id": 1,
        "title": "€ 29"
    }, {
        "id": 1,
        "title": "€ 48"
    }]
}, {
    "id": 4,
    "name": "galata",
    "title": "Galata",
    "hero": "Navigation. History. A Full Immersion.",
    "abstract": "The largest and most innovative sea museum in the Mediterranean: a reconstruction of a 16th century, 33-metre long Genoa galley and an 18th century brigantine-schooner, six thousand original objects in the twenty three display cases of Galata for a full immersion in navigation history.",
    "bodycol1": "",
    "video": null,
    "images": ["img/tweet-02.jpg", "img/polaroid-04.jpg"],
    "visits": [4],
    "duration": "2 h",
    "fares": [{
        "id": 1,
        "title": "Free"
    }, {
        "id": 1,
        "title": "€ 12"
    }, {
        "id": 1,
        "title": "€ 17"
    }]
}, {
    "id": 5,
    "name": "galata-pack",
    "title": "Galata Pack",
    "hero": "Explore. Sea. Museum.",
    "abstract": "Find out what binds man to the sea both above and beneath the water. Visit the Acquario di Genova and then go on to explore the Galata Museo del Mare &#8211; the largest Maritime Museum in the Mediterranean.",
    "bodycol1": "",
    "video": null,
    "images": ["img/tweet-02.jpg", "img/polaroid-05.jpg"],
    "visits": [1,4,5],
    "duration": "4 h",
    "fares": [{
        "id": 1,
        "title": "Free"
    }, {
        "id": 1,
        "title": "€ 23"
    }, {
        "id": 1,
        "title": "€ 37"
    }]
}, {
    "id": 6,
    "name": "biosphere",
    "title": "Biosphere",
    "hero": "Tropical. Forest. Reptiles.",
    "abstract": "A \"drop\" of steel and glass suspended over the sea, which hosts vegetable and animal species coming from tropical forests threatened by human exploitation. You will be able to view them from close, admire their fragile beauty and understand how their survival also depends on you.",
    "bodycol1": "",
    "video": null,
    "images": ["img/tweet-02.jpg", "img/polaroid-06.jpg"],
    "visits": [3],
    "duration": "45 min",
    "fares": [{
        "id": 1,
        "title": "Free"
    }, {
        "id": 1,
        "title": "€ 3"
    }, {
        "id": 1,
        "title": "€ 5"
    }]
}, {
    "id": 7,
    "name": "planet-pack",
    "title": "Planet Pack",
    "hero": "Tropical Garden. Wings Flapping. Museum.",
    "abstract": "Plunge into some of the world&#8217;s most fascinating humid environments and explore the Acquario di Genova, Tropical Garden &#8211; Wings Flapping, the Museo Nazionale dell&#8217;Antartide  and the Biosfera.",
    "bodycol1": "",
    "video": null,
    "images": ["img/tweet-02.jpg", "img/polaroid-01.jpg"],
    "visits": [1,3,6],
    "duration": "4 h",
    "fares": [{
        "id": 1,
        "title": "Free"
    }, {
        "id": 1,
        "title": "€ 20"
    }, {
        "id": 1,
        "title": "€ 33"
    }]
}, {
    "id": 8,
    "name": "antartic-museum",
    "title": "Antartic Museum",
    "hero": "Learn. Charm. Antartic.",
    "abstract": "To learn about an unknown and charming land. At the discovery of the Italian base in Antarctica and the most suggestive scenarios through films, reconstructions and scientific finding. Interactive and advanced displays will allow you to discover the last uncontaminated continent and to follow the activity of Italian researches in Antarctica, the vast natural laboratory to study environmental and climatic problems.",
    "bodycol1": "",
    "video": null,
    "images": ["img/tweet-02.jpg", "img/polaroid-02.jpg"],
    "visits": [6],
    "duration": "45 min",
    "fares": [{
        "id": 1,
        "title": "Free"
    }, {
        "id": 1,
        "title": "€ 4"
    }, {
        "id": 1,
        "title": "€ 7"
    }]
}, {
    "id": 9,
    "name": "bigo-panoramic",
    "title": "Bigo Panoramic",
    "hero": "Big. Panoramic. Amusement.",
    "abstract": "A panoramic lift offering a 360° view on Porto Antico di Genova. Bigo was designed by architect Renzo Piano to offer you an amazing aerial view of one of the largest historical centres of Europe, rich of art.",
    "bodycol1": "",
    "video": null,
    "images": ["img/tweet-02.jpg", "img/polaroid-03.jpg"],
    "visits": [7],
    "duration": "10 min",
    "fares": [{
        "id": 1,
        "title": "Free"
    }, {
        "id": 1,
        "title": "€ 3"
    }, {
        "id": 1,
        "title": "€ 4"
    }]
}, {
    "id": 10,
    "name": "sleep-with-sharks",
    "title": "Sleep with the sharks",
    "hero": "Sleep With The Sharks.",
    "abstract": "Spend a night with the sharks. Sleep right in front of their tank and then see the transformation Acquario undergoes at dawn. If you are aged between 7 and 13, why not come and join our expert guides.<!-- Accompanied by one of our experts, explore a very particular route through one of the most extensive exhibits of aquatic bio-diversity in Europe. At the end of the 90 minute guided tour, you can continue your visit on your own. -->",
    "bodycol1": "",
    "video": null,
    "images": ["img/tweet-02.jpg", "img/polaroid-03.jpg"],
    "visits": [7],
    "duration": "night",
    "fares": [{
        "id": 1,
        "title": "Free"
    }, {
        "id": 1,
        "title": "€ 34"
    }, {
        "id": 1,
        "title": "€ 44"
    }]
}]
