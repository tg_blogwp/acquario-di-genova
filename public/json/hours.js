[{
    "id": 1,
    "type": "mid-season",
    "title": "from 8:30am to 9pm last admittance 7pm"
}, {
    "id": 2,
    "type": "mid-season",
    "title": "from 9am to 8pm last admittance 6pm"
}, {
    "id": 3,
    "type": "mid-season",
    "title": "from 9am to 6pm last admittance 4pm"
}, {
    "id": 4,
    "type": "low-season",
    "title": "from 9am to 6pm last admittance 4pm"
}, {
    "id": 5,
    "type": "low-season",
    "title": "from 9:30am to 8pm last admittance 6pm"
}, {
    "id": 6,
    "type": "high-season",
    "title": "from 8:30am to 10:30pm last admittance 8:30pm"
}]
