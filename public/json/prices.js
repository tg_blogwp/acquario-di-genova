[{
    "fare": "Adult",
    "price": "5,00&euro;",
    "info": null
}, {
    "fare": "Children 4-12 years",
    "price": "3,50&euro;",
    "info": null
}, {
    "fare": "Infant 0-3 years",
    "price": "Free",
    "info": null
}, {
    "fare": "Reduced",
    "price": "3,50&euro;",
    "info": "Reduced ticket is valid for Military personel and Senior (over 65 years old). Disabled persons and invalids with paper certification issued by the appropriate  offices. The assistant of 100% disabled persons is entitled to a free ticket."
}, {
    "fare": "Groups minimum 20 people",
    "price": "3,50&euro;",
    "info": null
}, {
    "fare": "School",
    "price": "3,50&euro;",
    "info": "2 teachers get a free ticket each 15 students."
}]
