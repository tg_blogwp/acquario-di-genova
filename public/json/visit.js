{
    "name": "biosphere",
    "label": "Biosphere",
    "hero": "Tropical. Forest. Reptiles.",
    "title": "Biosphere",
    "subtitle": "An ecosystem of million years ago",
    "abstract": "<p>A &#8220;drop&#8221; of steel and glass suspended over the sea, which hosts vegetable and animal species coming from tropical forests threatened by human exploitation.</p>",
    "body": "<p>A &#8220;drop&#8221; of steel and glass suspended over the sea, which hosts vegetable and animal species coming from tropical forests threatened by human exploitation. You will be able to view them from close, admire their fragile beauty and understand how their survival also depends on you.</p>",
    "video": null,
    "image": "img/biosfera-01.jpg",
    "link": null,
    "items": [{
        "name": "a-drop-of-steel",
        "title": "The Biosphere. A Botanic Garden In Genova.",
        "subtitle": "A drop of steel and glass suspended over the sea.",
        "abstract": "<p>Which hosts vegetable and animal species coming from tropical forests threatened by human exploitation. You will be able to view them from close, admire their fragile beauty and understand how their survival also depends on you. You will also be able to see rare specimen of tropical trees: in addition to the famous ferns, perhaps the largest in the world cultivated inside vase, there are also some other plants traditionally exploited by men, such as gum, coffee and cocoa. This small but rich botanical garden where birds, amphibians and reptiles circulate freely, represents the beauty, complexity and fragility of tropical forests.</p>",
        "image": "img/biosfera-02.jpg",
        "link": null,
        "showbuycta": true,
        "template": "adg.partials.shared.text-center"
    }, {
        "name": "scarlett-ibis",
        "title": "Elducimus Ruber",
        "subtitle": "Scarlett Ibis",
        "abstract": "<p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo.</p>",
        "image": "img/biosfera-03.jpg",
        "link": null,
        "showbuycta": true,
        "template": "adg.partials.shared.text-left"
    }, {
        "name": "scarlett-ibis",
        "title": "Lagonosticta Senegala",
        "subtitle": "Astro Amaranto",
        "abstract": "<p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo.</p>",
        "image": "img/biosfera-04.jpg",
        "link": null,
        "showbuycta": true,
        "template": "adg.partials.shared.text-right"
    }, {
        "name": "the-orchids",
        "title": "Phalaenopsis amabilis, Dendrobium compactum",
        "subtitle": "The Orchids",
        "abstract": "<p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo.</p>",
        "image": "img/biosfera-05.jpg",
        "link": null,
        "showbuycta": true,
        "template": "adg.partials.shared.text-center"
    }, {
        "name": "botanic-garden",
        "title": "A &#8220,drop&#8221, of steel and glass suspended over the sea",
        "subtitle": "A Botanic Garden in Genova",
        "abstract": "<p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo.</p>",
        "image": "img/biosfera-06.jpg",
        "link": null,
        "news": {
            "title": "Mata Mata a new species appear in the garden",
            "image": "img/news-02.jpg",
            "link": [{
                "template": "adg.partials.shared.buttons.accent",
                "title": "Read more",
                "uri": "#", "target": "_self"
            }]
        },
        "showbuycta": true,
        "template": "adg.partials.shared.text-center-closure"
    }, {
        "name": "plan",
        "title": "Timetables &amp; Prices",
        "subtitle": "Plan Your Visit",
        "abstract": "",
        "image": null,
        "showbuycta": false,
        "items": null,
        "template": "adg.partials.shared.plan-visit"
    }, {
        "name": "visits",
        "title": "",
        "subtitle": "Not only the Acquarium",
        "showbuycta": true,
        "items": null,
        "template": "adg.partials.shared.more-visits"
    }]
}
