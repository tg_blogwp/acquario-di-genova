{
    "name": "plan-your-visit",
    "label": "Tickets &amp; Prices",
    "hero": "Tickets &amp; Prices.",
    "title": "Acquario di Genova",
    "subtitle": "",
    "abstract": "",
    "body": "",
    "video": null,
    "image": "img/tickets-and-prices-01.jpg",
    "link": null,
    "items": [{
        "name": "buy-tickets",
        "title": "",
        "subtitle": "",
        "abstract": "",
        "image": null,
        "link": null,
        "showbuycta": false,
        "template": "adg.partials.shared.matrix"
    }, {
        "name": "more-tickets",
        "title": "",
        "subtitle": "Other Tickets",
        "showbuycta": false,
        "items": null,
        "template": "adg.partials.shared.more-tickets"
    }, {
        "name": "discover-liguria",
        "title": "",
        "subtitle": "Discover Liguria",
        "showbuycta": false,
        "items": null,
        "template": "adg.partials.shared.more-liguria"
    }]
}
