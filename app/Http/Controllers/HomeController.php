<?php

namespace App\Http\Controllers;

use Corcel\Post as Post;
use Corcel\Page as Page;

/**
 * HomeController short summary.
 *
 * HomeController description.
 *
 * @version 1.0
 * @author Luca Zampetti
 */
class HomeController extends Controller
{
    public function getIndex()
    {
        return view('adg.index');
    }
}