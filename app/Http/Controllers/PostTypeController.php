<?php

namespace App\Http\Controllers;

use App\Post as Post;
use App\Page as Page;
use App\Struttura as Struttura;

/**
 * PostTypeController short summary.
 *
 * PostTypeController description.
 *
 * @version 1.0
 * @author Luca Zampetti
 */
class PostTypeController extends Controller
{

    private static function objectizeArr($single){

        return json_decode(json_encode($single));

    }
    
    public function handleSlug($slug)
    {

        $post = $this::objectizeArr(Post::where(['post_name' => $slug])->published()->get());
        
        if (count($post) > 0) {

            $page = $this::objectizeArr(get_fields($post[0]->ID));

            if (trim($page->blade_template) != '') {
                return view('adg.'.$page->blade_template, ['page' => $page]);
            } else {
                return view('adg.'.$slug, ['post' => json_decode($post)]);
            }

        } else {

            return view('adg.404');
          
        }

    }
}