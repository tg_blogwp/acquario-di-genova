<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Artisaninweb\SoapWrapper\Facades\SoapWrapper;

class SoapController {

    public function demo()
    {
        // Add a new service to the wrapper
        SoapWrapper::add(function ($service) {

            $ct = file_get_contents($_SERVER['DOCUMENT_ROOT'].'\public\websolute1.p12');

            $service
                ->name('adgTmaster')
                ->wsdl('https://tickettest.artacom.it/biglietteria/services-ws/client/v1_1?wsdl')
                ->trace(true)                                                   // Optional: (parameter: true/false)
                //->header()                                                      // Optional: (parameters: $namespace,$name,$data,$mustunderstand,$actor)
                //->customHeader($customHeader)                                   // Optional: (parameters: $customerHeader) Use this to add a custom SoapHeader or extended class                
                //->cookie()                                                      // Optional: (parameters: $name,$value)
                //->location()                                                    // Optional: (parameter: $location)
                ->certificate($ct)        // Optional: (parameter: $certLocation)
                //->cache(WSDL_CACHE_NONE)                                        // Optional: Set the WSDL cache
                ->options(['cassa' => 'websolute1', 'utente' => 'websolute','password' => 'websolute001']);   // Optional: Set some extra options
        });

        $data = ['cassa' => 'websolute1', 'utente' => 'websolute','password' => 'websolute001'];

        // Using the added service
        SoapWrapper::service('adgTmaster', function ($service) use ($data) {
            //var_dump($service->getFunctions());
            //var_dump($service->call('test', [$data]));

            var_dump($service->call('caricaListaEventi', [$data]));
            
        });
    }

}