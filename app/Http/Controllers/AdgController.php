<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Corcel\Post as Post;
use Corcel\Page as Page;

class AdgController extends Controller
{
    public static function elencoPost()
    {
	    $exposedPosts = array();
	    $exposedPages = array();

        $posts = Post::where('post_type', 'post')->published()->take(10)->get();
        $pages = Page::where('post_type', 'page')->published()->take(10)->get();

        foreach($posts as $post){
    	    $exposedCont = $post['original'];
    	    array_push($exposedPosts, $exposedCont);
        }

        foreach($pages as $page){
    	    $exposedCont = $page['original'];
    	    array_push($exposedPages, $exposedCont);
        }

        return array($exposedPosts,$exposedPages);
    }

    public static function singleItem($slug, $type = 'post')
    {
        $output = Post::where(['post_type' => $type, 'post_name' => $slug])->published()->get();

        $renderedCont = array();

	        if(count($output) > 0 ){

		        foreach($output as $item){
		    	    $exposedCont = $item['original'];
		    	    array_push($renderedCont, $exposedCont);
		        }

	        return array($renderedCont);

	    } else {
		    return array('Contenuto NON trovato');
	    }
    }
    public static function itemExist($slug){

        $output = Post::where(['post_name' => $slug])->published()->get();
        return (count($output) > 0?true:false);

    }
}
