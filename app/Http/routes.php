<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/plan-your-visit', function () {
    return view('adg.plan-your-visit');
});

//frontpage
Route::get('/', 'HomeController@getIndex');

// info - visits - experiences
Route::any('{slug}', 'PostTypeController@handleSlug')->where('slug', '(.*)?');

/*
Route::get('/', function () {
    return view('adg.index');
    // return view('adg.welcome');
});

Route::get('old', function () {
    return view('adg.oldindex');
});

Route::get('posts', function () {
    return view('adg.posts');
});
 */

/*
Route::get('{slug}', function ($slug) {
	$wpService = new \App\Http\Controllers\AdgController();
	if($wpService::itemExist($slug)){
    	return view('adg.single', ['slug' => $slug]);
    }else{
    	return View::make('adg.404');
    }

})->where('slug','[-A-Za-z0-9]+');
*/