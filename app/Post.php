<?php // File: app/Post.php

namespace App;

use Corcel\Post as Corcel;

class Post extends Corcel
{
    protected $connection = 'wordpress';
    // protected $postType = 'post';

    function mapMeta($item)
    {
    	$toitem = [
    		'_wp_page_template' 		=> 'template',
    		'abstract_entry' 			=> 'abstract',
    		'description_entry' 		=> 'description',
    		'test_entry' 				=> 'test',
    		'_thumbnail_id' 			=> 'thumbnail',
            'blade_template'            => 'template'
    	];
    	$tometas = [
    		'keywords_entry' 			=> 'keywords'
    	];
    	$metas = array();
    	if (isset($this->meta)) {
	    	foreach ($this->meta as $k => $v) {
	    		$key = $v->meta_key;
	    		$value = $v->meta_value;
	    		if (array_key_exists($key, $toitem)) {
	    			$item->$toitem[$key] = $value;
	    		} elseif (array_key_exists($key, $tometas)) {
	    			$metas[$tometas[$key]] = $value;
	    		} else {
	    			$metas[$key] = $value;
	    		}		        
	    	}
    	}
    	$item->metas = $metas;
    }

    public function map()
    {
        $item = (object) [
        	'id' 						=> $this->ID,
        	'parentId' 					=> $this->post_parent,
        	'author' 					=> $this->post_author,
        	'order' 					=> $this->menu_order,
        	'name' 						=> $this->post_name,       	
        	'title' 					=> $this->post_title,
        	'body' 						=> $this->post_content,        	
        	'type' 						=> $this->post_type,
        	'uri' 						=> $this->guid,
        	'status' 					=> $this->post_status,
        	'date' 						=> $this->post_date,
        	'dateGmt' 					=> $this->post_date_gmt
        ];
        $this->mapMeta($item);
        return $item;        
    }

    public static function getHtmlColumn($html, $column) {
        return $html;
    }
}

?>