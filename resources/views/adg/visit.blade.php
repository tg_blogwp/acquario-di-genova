<?php
/**
 * Template Name: Visit
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */

if(isset($page->sezione_struttura)) $page->items  = $page->sezione_struttura;

?>
@extends('adg.layouts.master', ['page' => $page])

@section('header')
    @include('adg.partials.shared.jumbo', ['item' => $page])
    @include('adg.partials.shared.hours')
    @include('adg.partials.shared.header')
@endsection

@section('content')
    @if(is_array($page->items))
        @foreach ($page->items as $index => $sub)
            @include($sub->template, ['item' => $sub])
        @endforeach
    @endif
@endsection

@section('footer')
    @include('adg.partials.shared.footer')
@endsection