<?php
/**
 * Template Name: Fascia Altre Tipologie Biglietti
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
if (!isset($item)) {
    $item = (object) [
        'name' => 'visits',
        'subtitle' => 'Other Tickets',
        'showbuycta' => true,
        'items' => [
            $visit = (object) [
            'name' => 'name',
            'title' => 'Title',
            'abstract' => 'Abstract',
            'image' => 'http://placehold.it/210x210'
            ],
            $visit,
            $visit
        ] 
    ];
}
if (!isset($item->items) || !$item->items) {
    $json = file_get_contents("json/more-tickets.js");
    $item->items = json_decode($json);    
}
?>
@if ($item->items && count($item->items) > 0)
<section class="tickets {{$item->name}}" id="{{$item->name}}">
    <div class="container-fluid">
        <div class="row text-center" style="padding-bottom: 20px;">
            <div class="col-sm-12">
                <div class="wow fadeIn FUP">
                    <h2 style="font-size: 60px;">{{$item->subtitle}}</h2>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach ($item->items as $index => $sub)              
            <div class="col-sm-6 col-md-4">
                <div class="box wow fadeIn" data-wow-delay="{{(150 * $index)}}ms" data-wow-offset="-400">
                    <span class="media">
                        <span class="picture">
                            <img src="{{$sub->image}}" />
                            <span class="overlay"></span>
                        </span>
                    </span>
                    <span class="headline-bottom">
                        <span class="title">{!!$sub->title!!}</span>
                        <span class="description">{{$sub->abstract}}</span>
                    </span>
                </div>
                <div class="more-content clearfix">
                    <h4>{!!$sub->subtitle!!}</h4>
                    <dl class="dl-horizontal">
                        @foreach ($sub->items as $index => $dl)                       
                            <dt>{{$dl->title}}</dt>
                            <dd>{{$dl->price}}</dd>
                        @endforeach
                    </dl>
                    @foreach ($sub->notes as $index => $note)                       
                        <p class="accent">{!!$note!!}</p>
                    @endforeach
                </div>            
                @if ($sub->link)
                    @foreach ($sub->link as $index => $link)
                        @include($link->template, ['link' => $link])
                    @endforeach                        
                @endif
            </div>
            @endforeach
        </div>
        @if (isset($item->showbuycta) && $item->showbuycta)
        <div class="row text-center">
            <div class="col-xs-12 padding-top-1">
                @include('adg.partials.shared.buttons.sticky-transparent')
            </div>
        </div>
        @endif
    </div>
</section>
@endif