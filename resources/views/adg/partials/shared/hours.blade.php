<?php
/**
 * Template Name: Fascia Orario e Acquista
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
if (!isset($item) || !is_array($item)) {
    $item = (object) [
        'title' => 'Acquarium Hours',
        'abstract' => 'Open today from 9:30 a.m. to 8:00 p.m.'
    ];
}
?>
<section class="hours-buy">
    <div class="inner">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 hours">
                    <i class="icon-clock"></i>
                    <h4>{{$item->title}}</h4>
                    <p>{{$item->abstract}}</p>
                </div>
                <div class="col-sm-6 text-right tickets">
                    @include('adg.partials.shared.buttons.sticky-transparent')
                </div>
            </div>
        </div>
    </div>
</section>