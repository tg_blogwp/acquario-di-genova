<?php
/**
 * Template Name: Fascia Menu Routes
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
if (!isset($item)) {
    $item = (object) [
        'label' => 'Label',
        'items' => [
            $sub = (object) [
                'name' => 'name',
                'label' => 'Label'
            ],
            $sub,
            $sub,
            $sub
        ]
    ];
}
?>
@if ($item->items && count($item->items) > 0)
<div sticky=".menu-sections">
    <section class="menu-sections">
        <div class="inner">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div style="position: relative;">
                            <h3>{{$item->label}}</h3>
                            <ul class="nav nav-sections">
                                @foreach ($item->items as $index => $sub)
                                    @if (isset($sub->label))
                                        <li ng-class="{ active: section == '{{$sub->name}}', before: isBeforeActiveIndex({{$index}}) }">
                                            <a href="#/{{$sub->name}}" page-nav>{{$sub->label}}</a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endif