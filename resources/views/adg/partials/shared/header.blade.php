<?php
/**
 * Template Name: Header
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
use Corcel\Menu as Menu;
$menu = Menu::get(); // slug('primary')->first(); // 
// var_dump($menu);
$menuArray = array();
if (isset($menu->nav_items)) {
    /*
    foreach ($menu->nav_items as $item) {
        // ....
        'post_title'    => '....', // Nav item name
        'post_name'     => '....', // Nav item slug
        'guid'          => '....', // Nav full url, influent by permalinks
        // ....
    }
    */
    foreach ($menu->nav_items as $item) {
        $parent_id = $item->meta->_menu_item_menu_item_parent;
        $menuArray[$parent_id][] = $item;
    }
    foreach ($menuArray[0] as $item) {
        echo '.. menu item main ..';
        if (isset($menuArray[$item->ID])) {
            foreach($menuArray[$item->ID] as $subItem) {
                echo '.. show sub menu item ..';
            }
        }
    }
}
?>
<div class="layer">
    <div class="menu-xs visible-xs" ng-class="{ opened: menuOpened }" >
        <ul class="nav-xs" role="navigation">
            <li ng-click="menu = 0">
                <a href="#" ng-class="{ active: menu == 0 }">
                    <span>Visit</span>
                </a>
                <ul ng-class="{ opened: menu == 0 }">
                    <li>
                        <a href="/genova-acquarium">
                            <span>Genova Acquarium</span>
                        </a>
                    </li>
                    <li>
                        <a href="/secret-acquarium">
                            <span>Secret Acquarium</span>
                        </a>
                    </li>
                    <li>
                        <a href="/biosfera">
                            <span>Biosfera</span>
                        </a>
                    </li>
                    <li>
                        <a href="/galata-museum">
                            <span>Galata Museum</span>
                        </a>
                    </li>
                    <li>
                        <a href="/submarine-s518">
                            <span>Submarine NS518</span>
                        </a>
                    </li>
                    <li>
                        <a href="/antartic-museum">
                            <span>Antartic Museum</span>
                        </a>
                    </li>
                    <li>
                        <a href="/bigo-panoramic">
                            <span>Bigo Panoramic</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li ng-click="menu = 1">
                <a href="#" ng-class="{ active: menu == 1 }">
                    <span>Experience</span>
                </a>                                    
                <ul ng-class="{ opened: menu == 1 }">
                    <li>
                        <a href="/acquario-village">
                            <span>Acquario Village</span>
                        </a>
                    </li>
                    <li>
                        <a href="/acquario-planet">
                            <span>Acquario Planet</span>
                        </a>
                    </li>
                    <li>
                        <a href="/galat-acquario">
                            <span>GalatAcquario</span>
                        </a>
                    </li>
                    <li>
                        <a href="/acquario-secret">
                            <span>Acquario Secret</span>
                        </a>
                    </li>
                    <li>
                        <a href="/guided-acquarium">
                            <span>Guided Acquarium</span>
                        </a>
                    </li>
                    <li>
                        <a href="/sleep-with-the-sharks">
                            <span>Sleep with the Sharks</span>
                        </a>
                    </li>
                    <li>
                        <a href="/crocier-acquario">
                            <span>CrocierAcquario</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="/info">
                    <span>Info</span>
                </a>
            </li>
            <li>
                <a href="/education">
                    <span>Education</span>
                </a>
            </li>
            <li>
                <a href="/events">
                    <span>Events</span>
                </a>
            </li>
            <li>
                <a href="/plan-your-visit">
                    <span>Plan Your Visit</span>
                </a>
            </li>
        </ul>
        <ul class="subnav-xs" role="navigation">
            <li>
                <a href="#">Join our Newsletter</a>
            </li>
            <li>
                <a href="/blog">Blog</a>
            </li>
            <li>
                <a href="#">Sign In</a>
            </li>
        </ul>
        <div class="container-fluid">
            <div class="divline"></div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="nav-group clearfix" ng-class="{ 'opened-left': langOpened, 'opened-right': searchOpened }">
                        <div class="nav-left">
                            <ul class="subnav-xs langnav" role="navigation">
                                <li ng-click="langOpened = !langOpened; searchOpened = false">
                                    <a href="#">English <i class="icon pull-right" ng-class="{ 'icon-arrow-bottom': !langOpened, 'icon-arrow-top': langOpened }"></i></a>
                                    <ul ng-class="{ opened: langOpened }">
                                        <li>
                                            <a href="/it/">
                                                <span>Italiano</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/en/">
                                                <span>English</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>                 
                        </div>    
                        <div class="nav-right">   
                            <button type="button" class="btn btn-search btn-block" ng-click="searchOpened = !searchOpened; langOpened = false">Search <i class="icon-search"></i></button>   
                        </div>
                    </div>
                </div>
            </div>
            <a href="/plan-your-visit" class="btn btn-white btn-block">
                <i class="icon-calendar"></i>
                <span>PLAN YOUR VISIT</span>
                <i class="icon-arrow-right"></i>
            </a>
        </div>
    </div>
    <div class="container-fluid header">
        <div class="row">
            <div class="col-sm-3">
                <a href="/" title="Acquario di Genova"><img src="/img/logo-acquario-di-genova@2x.png" class="logo" title="Acquario di Genova" /></a>
                <button class="btn btn-menu visible-xs pull-right" type="button" ng-click="menuOpened = !menuOpened"><span>Menu</span><i class="icon icon-menu" ng-class="{ 'icon-menu': !menuOpened, 'icon-close': menuOpened }"></i></button>
            </div>
            <div class="col-sm-9 hidden-xs">
                <div class="vertical menu-container">
                    <div class="center">
                        <ul class="menu" role="navigation">
                            <li ng-mouseenter="menu = 0">
                                <a href="#">
                                    <span>Visit</span>
                                </a>
                                <ul ng-class="{ show: menu == 0 }" ng-mouseleave="menu = null">
                                    <li>
                                        <img src="/img/polaroid-01.jpg" />
                                        <a href="/genova-acquarium">
                                            <span>Genova Acquarium</span>
                                        </a>
                                    </li>
                                    <li>
                                        <img src="/img/polaroid-01.jpg" />
                                        <a href="/secret-acquarium">
                                            <span>Secret Acquarium</span>
                                        </a>
                                    </li>
                                    <li>
                                        <img src="/img/polaroid-02.jpg" />
                                        <a href="/biosfera">
                                            <span>Biosfera</span>
                                        </a>
                                    </li>
                                    <li>
                                        <img src="/img/polaroid-03.jpg" />
                                        <a href="/galata-museum">
                                            <span>Galata Museum</span>
                                        </a>
                                    </li>
                                    <li>
                                        <img src="/img/polaroid-04.jpg" />
                                        <a href="/submarine-ns518">
                                            <span>Submarine NS518</span>
                                        </a>
                                    </li>
                                    <li>
                                        <img src="/img/polaroid-05.jpg" />
                                        <a href="/antartic-museum">
                                            <span>Antartic Museum</span>
                                        </a>
                                    </li>
                                    <li>
                                        <img src="/img/polaroid-06.jpg" />
                                        <a href="/bigo-panoramic">
                                            <span>Bigo Panoramic</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li ng-mouseenter="menu = 1">
                                <a href="#">
                                    <span>Experience</span>
                                </a>                                    
                                <ul ng-class="{ show: menu == 1 }" ng-mouseleave="menu = null">
                                    <li>
                                        <img src="/img/polaroid-01.jpg" />
                                        <a href="/acquario-village">
                                            <span>Acquario Village</span>
                                        </a>
                                    </li>
                                    <li>
                                        <img src="/img/polaroid-01.jpg" />
                                        <a href="/acquario-planet">
                                            <span>Acquario Planet</span>
                                        </a>
                                    </li>
                                    <li>
                                        <img src="/img/polaroid-02.jpg" />
                                        <a href="/galat-acquario">
                                            <span>GalatAcquario</span>
                                        </a>
                                    </li>
                                    <li>
                                        <img src="/img/polaroid-03.jpg" />
                                        <a href="/acquario-secret">
                                            <span>Acquario Secret</span>
                                        </a>
                                    </li>
                                    <li>
                                        <img src="/img/polaroid-04.jpg" />
                                        <a href="/guided-acquarium">
                                            <span>Guided Acquarium</span>
                                        </a>
                                    </li>
                                    <li>
                                        <img src="/img/polaroid-05.jpg" />
                                        <a href="/sleep-with-the-sharks">
                                            <span>Sleep with the Sharks</span>
                                        </a>
                                    </li>
                                    <li>
                                        <img src="/img/polaroid-06.jpg" />
                                        <a href="/crocier-acquario">
                                            <span>CrocierAcquario</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="/info">
                                    <span>Info</span>
                                </a>
                            </li>
                            <li>
                                <a href="/education">
                                    <span>Education</span>
                                </a>
                            </li>
                            <li>
                                <a href="/events">
                                    <span>Events</span>
                                </a>
                            </li>
                            <li>
                                <a href="/plan-your-visit">
                                    <span>Plan Your Visit</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--
            <div class="col-sm-3 hidden-xs">
                <div class="vertical">
                    <div class="center">
                        <a href="/plan-your-visit" class="btn btn-secondary btn-block btn-shadow">
                            <i class="icon-calendar"></i>
                            <span>PLAN YOUR VISIT</span>
                            <i class="icon-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            -->
            <div class="submenu hidden-xs">
                <ul class="menu sub" role="navigation">
                    <li>
                        <a href="#">Join our Newsletter</a>
                    </li>
                    <li>
                        <a href="#">English <i class="icon-chevron-down"></i></a>
                    </li>
                    <li>
                        <a href="#">Sign In</a>
                    </li>
                    <li>
                        <a href="/blog">Blog</a>
                    </li>
                    <li>
                        <a href="/search"><i class="icon-search"></i></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="divline"></div>
    </div>
</div>
<?=get_header()?>