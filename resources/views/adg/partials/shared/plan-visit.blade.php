<?php
/**
 * Template Name: Fascia Tabella Visit
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
if (!isset($item)) {
    $item = (object) [
        'name' => 'name',
        'title' => 'Title',
        'subtitle' => 'Subtitle',
        'image' => null
    ];
}

if (!isset($item->infos)) {
    $item->infos = [
        'Biosfera will be closed the 10<sup>th</sup> and 11<sup>th</sup> of September.',
        'For additional information, visit the <a href="#" class="link">Biosfera&#8217;s website</a>.'
    ];
}

$json = file_get_contents("json/timetable.js");
$timetable = json_decode($json);

$json = file_get_contents("json/prices.js");
$prices = json_decode($json);

$labels = (object) [
    'from' => 'From',
    'to' => 'To',
    'lastAdmission' => 'Last Admission',
    'ticketsPrices' => 'Tickets prices',
    'pleaseNote' => 'Please Note'
];
?>
<section class="section-tables {{$item->name}}" id="{{$item->name}}">
    <div class="background wow fadeIn FUP" data-wow-delay="250ms">
        <img src="{{$item->image}}" />
    </div>
    <div class="container-fluid">
        <div class="row text-center">
            <div class="col-sm-12">
                <div class="wow fadeIn FUP" data-wow-delay="250ms">
                    <h3 class="text-serif">{{$item->title}}</h3>
                    <h1>{{$item->subtitle}}</h1>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-6">
                @if ($timetable)
                <table class="table wow fadeIn FUP" data-wow-delay="450ms">
                    <thead>
                        <tr>
                            <th></th>
                            <th>{{$labels->from}}</th>
                            <th>{{$labels->to}}</th>
                            <th>{{$labels->lastAdmission}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($timetable as $index => $time)
                        <tr>
                            <td class="text-left">{{$time->month}}</td>
                            <td>{{$time->from}}</td>
                            <td>{{$time->to}}</td>
                            <td>{{$time->last}}</td>                            
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif
            </div>
            <div class="col-md-6">
                @if ($prices)
                <table class="table wow fadeIn FUP" data-wow-delay="650ms">
                    <thead>
                        <tr>
                            <th colspan="2">{{$labels->ticketsPrices}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($prices as $index => $price)
                        <tr>
                            <td class="text-left">{{$price->fare}} 
                                @if (isset($price->info))
                                <span>{{str_repeat('*', isset($i) ? ++$i : $i = 1 )}}</span>
                                @endif
                            </td>
                            <td class="text-right">{{$price->price}}</td>      
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>
        <div class="row text-center">
            <div class="col-xs-12">
                <div class="wow fadeIn FUP" data-wow-delay="850ms">
                    @if (isset($item->infos))
                        @foreach ($item->infos as $index => $info)
                            <p><span class="accent">{{$labels->pleaseNote}}</span> <span>{!!$info!!}</span></p>
                        @endforeach
                    @endif
                    @if ($prices)
                        @foreach ($prices as $index => $price)
                            @if (isset($price->info))
                                <small><span>{{str_repeat('*', isset($i) ? ++$i : $i = 1 )}}</span> {{$price->info}}</small><br>
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
