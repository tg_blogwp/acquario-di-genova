<?php
/**
 * Template Name: Fascia Jumbo
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
if (!isset($item)) {
    $item = (object) [
        'name' => 'name',
        'title' => 'Title',
        'hero' => 'Hero',
        'video' => null,
        'image' => 'http://placehold.it/1600x684',
        'link' => [
            (object) [
                'title' => 'Title',
                'template' => 'adg.partials.shared.buttons.transparent',
                'uri' => '#',
                'target' => '_self'
            ]
        ]
    ];
}
?>
<section class="jumbo {{$item->name}}">
    @if ($item->video)
    <div background class="transition">
        <video src="{{$item->video}}" autoplay loop></video>
    </div>
    @elseif ($item->image)
    <div class="background">
        <picture style="background-image:url({{$item->image}});"></picture>
    </div>
    @endif    
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="vertical">
                    <div class="center">
                        <h3 class="text-serif wow fadeIn FUP">{{$item->title}}</h3>
                        <h1 class="wow fadeIn FUP" data-wow-delay="150ms">{{$item->hero}}</h1>
                        @if ($item->link)
                            @foreach ($item->link as $index => $link)
                                @include($link->template, ['link' => $link])
                            @endforeach                        
                        @endif
                    </div>
                </div>  
            </div>
        </div>
    </div>    
</section>
