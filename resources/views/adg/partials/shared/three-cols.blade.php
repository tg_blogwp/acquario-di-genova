<?php
/**
 * Template Name: Fascia Testo 3 Colonne
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
use App\Post as Post;
if (!isset($item)) {
    $item = (object) [
        'name' => 'name',
        'subtitle' => 'Subtitle',
        'bodycol1' => 'Body Col 1',
        'bodycol2' => 'Body Col 2',
        'image' => null,
        'link' => [
            (object)[
                'template' => 'adg.partials.shared.buttons.primary',
                'title' => 'Button',
                'uri' => '#'
            ]
        ],
        'showbuycta' => false
    ];
}

?>
<a name="/{{$item->name}}">&nbsp;</a>
@if ($item->items && count($item->items) > 0)
    @foreach ($item->items as $index => $sub)
        <a name="/{{$item->name}}/{{$sub->name}}">&nbsp;</a>
    @endforeach
@endif

<section class="section-3-columns {{$item->name}}" id="{{$item->name}}">
    @if ($item->image)
    <div class="background wow fadeIn FUP" data-wow-delay="250ms">
        <img src="{{$item->image}}" />
    </div>
    @endif
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="wow fadeIn FUP" data-wow-delay="250ms">
                    <h2>{!!$item->subtitle!!}.</h2>
                </div>
            </div>
            <div class="col-sm-6 col-md-8">
                <div class="row text-justify">
                    <div class="col-md-6">
                        <div class="wow fadeIn FUP" data-wow-delay="450ms">
                            {!!$item->bodycol1!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="wow fadeIn FUP" data-wow-delay="650ms">
                            {!!$item->bodycol2!!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-xs-12">
                <div class="buttons wow fadeIn" data-wow-delay="850ms">
                    @if ($item->link)
                        @foreach ($item->link as $index => $link)
                            @include($link->template, ['link' => $link])
                        @endforeach                        
                    @endif
                    @if (isset($item->showbuycta) && $item->showbuycta)
                        @include('adg.partials.shared.buttons.sticky-transparent')
                    @endif
                </div>
            </div>
        </div>
        @if ($item->items && count($item->items) > 0)
        <div class="row"> 
            <div class="col-sm-12">
                <ul class="nav nav-chapters wow fadeIn FUP" data-wow-delay="450ms">
                    @foreach ($item->items as $index => $sub)
                    <li ng-class="{ active: isSubActive('{{$item->name}}', '{{$sub->name}}', {{$index}}) }">
                        <a href="#/{{$item->name}}/{{$sub->name}}" page-nav>{{$sub->label}}</a>
                    </li>
                    @endforeach
                </ul>                
            </div>
        </div>
        <div class="wow fadeIn FUP" data-wow-delay="650ms">
            @foreach ($item->items as $index => $sub)
                <div class="row nav-chapters-content text-justify {{$item->name}}" id="{{$item->name}}/{{$sub->name}}" ng-if="isSubActive('{{$item->name}}', '{{$sub->name}}', {{$index}})">
                    @if ($sub->template)
                        @include($sub->template, ['subItem' => $sub])
                    @else
                    <div class="col-md-8">
                        <h3>{!!$sub->subtitle!!}</h3>
                        {!!$sub->bodycol1!!}
                    </div>
                    <div class="col-md-4">
                        {!!$sub->bodycol2!!}
                    </div>
                    @endif
                </div>
            @endforeach        
        </div>
        @endif
    </div>
</section>
