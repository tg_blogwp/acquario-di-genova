<?php
/**
 * Template Name: Footer
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
?>
    <div class="container-fluid">
        @include('adg.partials.shared.buttons.sticky')
        <div class="row text-center">
            <div class="col-sm-12">
                <img src="/img/loghi.jpg" />
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div style="width:100%; height:1px; background:white"></div>
    </div>
    <div class="container-fluid">
        <div class="row text-center">
            <div class="col-sm-12">
                <br />
                <ul class="menu" role="navigation" style="display: inline-block; margin-bottom: 40px;">
                    <li>
                        <a href="/press-area">
                            <span>PRESS AREA</span>
                        </a>
                    </li>
                    <li>
                        <a href="/partnership">
                            <span>PARTNERSHIP</span>
                        </a>
                    </li>
                    <li>
                        <a href="/site-map">
                            <span>SITE MAP</span>
                        </a>
                    </li>
                    <li>
                        <a href="/privacy">
                            <span>PRIVACY</span>
                        </a>
                    </li>
                    <li>
                        <a href="/credits">
                            <span>CREDITS</span>
                        </a>
                    </li>
                </ul>
                <br />
                <small>
                    <p>Costa Edutainment S.p.A. Acquario di Genova | Area Porto Antico - Ponte Spinola - 16128 - Genova</p>
                    <p>Tel. 010/23451 - Fax 010/256160</p>
                </small>
            </div>
        </div>
    </div>
    <?=get_footer()?>