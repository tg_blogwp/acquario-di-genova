<?php
/**
 * Template Name: Sub Template 2 Colonne
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
use App\Post as Post;
if (!isset($subItem)) {
    $subItem = (object) [
        'subtitle' => 'Subtitle',
        'body' => 'Body'
    ];
}
?>
<div class="col-md-8">
    <h3>{!!$subItem->subtitle!!}</h3>
    {!!$subItem->bodycol1!!}
</div>
<div class="col-md-4">
    {!!$subItem->bodycol2!!}
</div>
