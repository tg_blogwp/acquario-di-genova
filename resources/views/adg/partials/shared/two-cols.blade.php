<?php
/**
 * Template Name: Fascia Testo 3 Colonne
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
use App\Post as Post;
if (!isset($item)) {
    $item = (object) [
        'name' => 'name',
        'subtitle' => 'Subtitle',
        'bodycol1' => 'Body Col 1',
        'bodycol2' => 'Body Col 2',
        'image' => null,
        'link' => null,
        'showbuycta' => false
    ];
}
?>
<section class="section-2-columns {{$item->name}}" id="{{$item->name}}">
    @if ($item->image)
    <div class="background wow fadeIn FUP" data-wow-delay="250ms">
        <img src="{{$item->image}}" />
    </div>
    @endif
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-8">
                <div class="wow fadeIn FUP" data-wow-delay="250ms">
                    <h2>{!!$item->subtitle!!}.</h2>
                    {!!$item->bodycol1!!}
                </div>
            </div>
            <div class="col-sm-4 text-justify">
                <div class="wow fadeIn FUP" data-wow-delay="450ms">
                    {!!$item->bodycol2!!}
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-xs-12">
                <div class="buttons wow fadeIn" data-wow-delay="850ms">
                    @if ($item->link)
                        @foreach ($item->link as $index => $link)
                            @include($link->template, ['link' => $link])
                        @endforeach                        
                    @endif
                    @if (isset($item->showbuycta) && $item->showbuycta)
                        @include('adg.partials.shared.buttons.sticky-transparent')
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
