<?php
/**
 * Template Name: Fascia Faq
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
use App\Post as Post;
if (!isset($item)) {
    $item = (object) [
        'name' => 'faq',
        'subtitle' => 'Faq',
        'bodycol1' => 'Body Col 1',
        'bodycol2' => 'Body Col 2',
        'items' => [
            $sub = (object) [
            'name' => 'name',
            'title' => 'Title',
            'abstract' => 'Abstract'
            ],
            $sub,
            $sub
        ] 
    ];
}
?>
<a name="/{{$item->name}}">&nbsp;</a>
<section class="section-3-columns {{$item->name}}" id="{{$item->name}}">
    @if ($item->image)
    <div class="background wow fadeIn FUP" data-wow-delay="250ms">
        <img src="{{$item->image}}" />
    </div>
    @endif
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="wow fadeIn FUP" data-wow-delay="250ms">
                    <h2>{!!$item->subtitle!!}.</h2>
                </div>
            </div>
            <div class="col-sm-6 col-md-8">
                <div class="row text-justify">
                    <div class="col-md-6">
                        <div class="wow fadeIn FUP" data-wow-delay="450ms">
                            {!!$item->bodycol1!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="wow fadeIn FUP" data-wow-delay="650ms">
                            {!!$item->bodycol2!!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if ($item->items && count($item->items) > 0)
        <div class="row">
            <div class="col-sm-12">
                <ul class="nav nav-faq">
                    @foreach ($item->items as $index => $sub)
                    <!--<a name="/{{$item->name}}/{{$sub->name}}">&nbsp;</a>-->
                    <li ng-class="{ active: section == '{{$item->name}}' && chapter == '{{$sub->name}}' }">
                        <a href="#/{{$item->name}}/{{$sub->name}}"><span>{{$sub->title}}</span></a>
                        <div class="row nav-faq-content text-justify" id="{{$item->name}}/{{$sub->name}}" ng-if="section == '{{$item->name}}' && chapter == '{{$sub->name}}'">
                            <div class="col-md-8">
                                {!!$sub->abstract!!}
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif
    </div>
</section>
