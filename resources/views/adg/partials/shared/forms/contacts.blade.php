<?php
/**
 * Template Name: Sub Template Form Contatti
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
if (!isset($subItem)) {
    $subItem = (object) [
        'name' => 'name',
        'title' => 'Title',
        'description' => 'Description',
        'body' => 'Body',
        'image' => null,
        'link' => null,
        'showbuycta' => false
    ];
}
?>
<div class="col-md-8">
    <h3>{!!$subItem->title!!}</h3>
    {!!$subItem->body!!}    
    <form name="{{$subItem->name}}" novalidate>
        <div class="container-fluid">
            <div class="row">
            form
            </div>
            <div class="row text-right">
            form
            </div>
        </div>
    </form>
</div>
<div class="col-md-4">
    {!!$subItem->description!!}
</div>
