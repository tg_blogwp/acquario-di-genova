<?php
/**
 * Template Name: Fascia Durata Percorsi
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
use App\Post as Post;
if (!isset($item)) {
    $item = (object) [
        'name' => 'duration',
        'subtitle' => 'Duration',
        'bodycol1' => 'Body Col 1',
        'bodycol2' => 'Body Col 2',
        'image' => '',
        'link' => null,
        'showbuycta' => false        
    ];
}

$json = file_get_contents("json/visits.js");
$visits = json_decode($json);

$json = file_get_contents("json/experiences.js");
$experiences = json_decode($json);

$json = file_get_contents("json/fares.js");
$fares = json_decode($json);

?>
<a name="/{{$item->name}}">&nbsp;</a>
@if ($visits && $experiences && $fares)
<script>
{{--
    var visits = {!!json_encode($visits)!!}}
    var experiences = {!!json_encode($experiences)!!}}
    var fares = {!!json_encode($fares)!!}}
--}}
var visits = [
    @foreach ($visits as $index => $visit)                    
        { id: {{$visit->id}}, name: "{{$visit->name}}", title: "{{$visit->title}}", },
    @endforeach
];
var experiences = [
    @foreach ($experiences as $index => $experience)                    
        { id: {{$experience->id}}, name: "{{$experience->name}}", title: "{{$experience->title}}", image: "{{$experience->images[1]}}", duration: "{{$experience->duration}}", visits: {!!json_encode($experience->visits)!!}, fares: {!!json_encode($experience->fares)!!}, },
    @endforeach
];
var fares = [
    @foreach ($fares as $index => $fare) 
        { id: {{$fare->id}}, index: {{$index}}, title: "{{$fare->title}}", }, 
    @endforeach
];
</script>
<section class="matrix {{$item->name}}" id="{{$item->name}}" ng-controller="MatrixCtrl">
    <div class="container-fluid">           
        <div class="row">
            <div class="col-sm-12">

                <!-- TABELLA ANGULAR -->                    
                <div resizeable-table="experiences.length" ng-if="experiences.length">
                    <table class="table table-matrix" style="position:relative;" fixed-header>
                        <thead>
                            <tr>
                                <th class="previous">
                                    <button type="button" class="btn btn-icon" ng-class="{ visible: hasPrevColumns() }" ng-click="showPrevTableColumn()"><i class="icon icon-arrow-left"></i></button>
                                </th>
                                <th class="col" ng-class="{ hover: experience.hover }" ng-mouseover="experience.hover = true" ng-mouseout="experience.hover = false" ng-click="doBuyTicket(experience.id)" ng-repeat="experience in experiences track by $index" ng-if="isTableColumnVisible($index)">
                                    <img ng-src="@{{experience.image}}" class="thumb" />
                                    <span ng-bind="experience.title"></span>
                                </th>
                                <th class="next">
                                    <button type="button" class="btn btn-icon" ng-class="{ visible: hasNextColumns() }" ng-click="showNextTableColumn()"><i class="icon icon-arrow-right"></i></button>
                                </th>               
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="visit in visits track by $index">
                                <td><span ng-bind="visit.title"></span></td>
                                <td class="col" ng-class="{ hover: experience.hover }" ng-mouseover="experience.hover = true" ng-mouseout="experience.hover = false" ng-click="doBuyTicket(experience.id)" ng-repeat="experience in experiences track by $index" ng-if="isTableColumnVisible($index)">
                                    <i class="icon icon-checkmark" ng-class="{ transparent: !hasVisit(experience, visit) }"></i>
                                </td>                                 
                            </tr>
                            <tr class="durations">
                                <td><span>Duration</span></td>
                                <td class="col" ng-class="{ hover: experience.hover }" ng-mouseover="experience.hover = true" ng-mouseout="experience.hover = false" ng-click="doBuyTicket(experience.id)" ng-repeat="experience in experiences track by $index" ng-if="isTableColumnVisible($index)"><span ng-bind="experience.duration"></span></td>                                   
                            </tr>
                            <tr class="fares" ng-repeat="fare in fares track by $index">
                                <td><span ng-bind="fare.title"></span></td>
                                <td class="col" ng-class="{ hover: experience.hover }" ng-mouseover="experience.hover = true" ng-mouseout="experience.hover = false" ng-click="doBuyTicket(experience.id)" ng-repeat="experience in experiences track by $index" ng-if="isTableColumnVisible($index)">
                                    <span ng-bind="experience.fares[fare.index].title"></span>
                                </td>
                            </tr>
                            <tr class="ctas">
                                <td>&nbsp;</td>
                                <td class="col" ng-class="{ hover: experience.hover }" ng-mouseover="experience.hover = true" ng-mouseout="experience.hover = false" ng-click="doBuyTicket(experience.id)" ng-repeat="experience in experiences track by $index" ng-if="isTableColumnVisible($index)">
                                    <a href="#" target="_blank" type="button" class="btn btn-buy"><i class="icon icon-add-to-cart"></i></a>
                                </td>
                            </tr>
                        </tbody>
                        {{--
                        <tfoot class="sticky">
                            <tr>
                                <th></th>
                                @foreach ($experiences as $index => $experience)
                                <th class="col" ng-class="{ hover: hover{{$index}} }" ng-mouseover="hover{{$index}} = true" ng-mouseout="hover{{$index}} = false" ng-click="doBuyTicket({{$experience->id}})">
                                    <img src="{{$experience->images[1]}}" class="thumb" />
                                    <span>{{$experience->title}}</span>
                                </th>
                                @endforeach      
                            </tr>
                        </tfoot>
                        --}}
                    </table>
                </div>

                {{--
                <!-- TABELLA LARAVEL -->
                <div class="visible-sm visible-md visible-lg" resizeable-table>
                    <table class="table table-matrix"><!-- sticky-footer -->
                        <thead>
                            <tr>
                                <th></th>
                                @foreach ($experiences as $index => $experience)
                                <th class="col" ng-class="{ hover: hover{{$index}} }" ng-mouseover="hover{{$index}} = true" ng-mouseout="hover{{$index}} = false" ng-click="doBuyTicket({{$experience->id}})">
                                    <img src="{{$experience->images[1]}}" class="thumb" />
                                    <span>{{$experience->title}}</span>
                                </th>
                                @endforeach      
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($visits as $index => $visit)
                            <tr>
                                <td>{{$visit->title}}</td>
                                @foreach ($experiences as $index => $experience)
                                <td class="col" ng-class="{ hover: hover{{$index}} }" ng-mouseover="hover{{$index}} = true" ng-mouseout="hover{{$index}} = false" ng-click="doBuyTicket({{$experience->id}})">
                                    <i class="icon icon-checkmark {{in_array($visit->id, $experience->visits) ? '' : 'transparent'}}"></i>                                
                                </td>
                                @endforeach  
                            </tr>
                            @endforeach                        
                            <tr class="durations">
                                <td>Duration</td>
                                @foreach ($experiences as $index => $experience)
                                    <td class="col" ng-class="{ hover: hover{{$index}} }" ng-mouseover="hover{{$index}} = true" ng-mouseout="hover{{$index}} = false" ng-click="doBuyTicket({{$experience->id}})"><span>{{$experience->duration}}</span></td>
                                @endforeach     
                            </tr>        
                            @foreach ($fares as $fi => $fare)
                            <tr class="fares">
                                <td>{{$fare->title}}</td>
                                @foreach ($experiences as $index => $experience)
                                <td class="col" ng-class="{ hover: hover{{$index}} }" ng-mouseover="hover{{$index}} = true" ng-mouseout="hover{{$index}} = false" ng-click="doBuyTicket({{$experience->id}})"><span>{{$experience->fares[$fi]->title}}</span></td>
                                @endforeach  
                            </tr>
                            @endforeach
                            <tr class="ctas">
                                <td>&nbsp;</td>
                                @foreach ($experiences as $index => $experience)
                                <td class="col" ng-class="{ hover: hover{{$index}} }" ng-mouseover="hover{{$index}} = true" ng-mouseout="hover{{$index}} = false" ng-click="doBuyTicket({{$experience->id}})"><a href="#" target="_blank" type="button" class="btn btn-buy"><i class="icon icon-add-to-cart"></i></a></td>
                                @endforeach  
                            </tr>
                        </tbody>
                        <!--<tfoot class="sticky">
                            <tr>
                                <th></th>
                                @foreach ($experiences as $index => $experience)
                                <th class="col" ng-class="{ hover: hover{{$index}} }" ng-mouseover="hover{{$index}} = true" ng-mouseout="hover{{$index}} = false" ng-click="doBuyTicket({{$experience->id}})">
                                    <img src="{{$experience->images[1]}}" class="thumb" />
                                    <span>{{$experience->title}}</span>
                                </th>
                                @endforeach      
                            </tr>
                        </tfoot>-->
                    </table>
                </div>
                <table class="table table-matrix visible-xs" ng-init="doShowExperience(0)">
                    <thead>
                        <tr>
                            <th class="text-right" style="width: 130px; padding: 0; vertical-align: middle;">
                                <button type="button" class="btn btn-icon" ng-click="doShowExperience(-1)"><i class="icon icon-arrow-left"></i></button>
                            </th>
                            @foreach ($experiences as $index => $experience)
                                <th class="col" ng-if="isExperienceVisible({{$index}})">
                                    <img src="{{$experience->images[1]}}" class="thumb" />
                                    <span>{{$experience->title}}</span>
                                </th>
                            @endforeach      
                            <th style="width: 30px; padding: 0; vertical-align: middle;">
                                <button type="button" class="btn btn-icon" ng-click="doShowExperience(1)"><i class="icon icon-arrow-right"></i></button>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($visits as $index => $visit)
                            <tr>
                                <td>{{$visit->title}}</td>
                                @foreach ($experiences as $index => $experience)
                                    <td class="col" ng-if="isExperienceVisible({{$index}})">
                                        <i class="icon icon-checkmark {{in_array($visit->id, $experience->visits) ? '' : 'transparent'}}"></i>          
                                    </td>
                                @endforeach  
                                <td>&nbsp;</td>
                            </tr>
                        @endforeach                        
                        <tr class="durations">
                            <td>Duration</td>
                            @foreach ($experiences as $index => $experience)
                                <td class="col" ng-if="isExperienceVisible({{$index}})"><span>{{$experience->duration}}</span></td>
                            @endforeach     
                            <td>&nbsp;</td>
                        </tr>        
                        @foreach ($fares as $fi => $fare)
                            <tr class="fares">
                                <td>{{$fare->title}}</td>
                                @foreach ($experiences as $index => $experience)
                                    <td class="col" ng-if="isExperienceVisible({{$index}})"><span>{{$experience->fares[$fi]->title}}</span></td>
                                @endforeach
                                <td>&nbsp;</td>  
                            </tr>
                        @endforeach
                        <tr class="ctas">
                            <td>&nbsp;</td>
                            @foreach ($experiences as $index => $experience)
                                <td class="col" ng-if="isExperienceVisible({{$index}})" ng-click="doBuyTicket({{$experience->id}})">
                                    <a href="#" target="_blank" type="button" class="btn btn-buy btn-block"><i class="icon icon-add-to-cart"></i></a>
                                </td>
                            @endforeach  
                            <td>&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
                --}}

            </div>
        </div>
    </div>
</section>
@endif
