<?php
/**
 * Template Name: Fascia Attrazioni Home e Correlati
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
if (!isset($item)) {
    $item = (object) [
        'name' => 'visits',
        'subtitle' => 'Not only the Aquarium',
        'showbuycta' => true,
        'items' => [
            $visit = (object) [
            'name' => 'name',
            'title' => 'Title',
            'abstract' => 'Abstract',
            'image' => 'http://placehold.it/210x210'
            ],
            $visit,
            $visit
        ] 
    ];
}
if (!isset($item->items) || !$item->items) {
    $json = file_get_contents("json/visits.js");
    $item->items = json_decode($json);    
}
?>
@if ($item->items && count($item->items) > 0)
<section class="more {{$item->name}}" id="{{$item->name}}">
    <div class="container-fluid">
        <div class="row text-center">
            <div class="col-sm-12">
                <div class="wow fadeIn FUP">
                    <h3 class="text-serif" style="font-size: 40px;">{{$item->subtitle}}</h3>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="slick-gallery" slick slick-options="slickOptions.more" arrows="true">
                @foreach ($item->items as $index => $sub)                
                <div class="col-sm-6 col-md-4">
                    @if (isset($sub->images))
                    <div pictures class="wow fadeIn" data-wow-delay="{{(150 * $index)}}ms">     
                        @foreach ($sub->images as $index => $image)                   
                        <a href="visits/{{$sub->name}}" class="box polaroid polaroid-{{$index}}">
                            <span class="media">
                                <span class="picture">
                                    <img src="{{$image}}" />
                                </span>
                            </span>
                        </a>
                        @endforeach
                    </div>                    
                    @endif
                    @if (isset($sub->image))
                    <a href="visits/{{$sub->name}}" class="box wow fadeIn" data-wow-delay="{{(150 * $index)}}ms">
                        <span class="media">
                            <span class="picture">
                                <img src="{{$sub->image}}" />
                            </span>
                        </span>
                    </a>
                    @endif
                    <div>
                        <h2>{!!$sub->title!!}</h2>
                        <p>{!!$sub->abstract!!}</p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @if (isset($item->showbuycta) && $item->showbuycta)
        <div class="row text-center">
            <div class="col-xs-12 padding-top-1">
                @include('adg.partials.shared.buttons.sticky-transparent')
            </div>
        </div>
        @endif
    </div>
</section>
@endif