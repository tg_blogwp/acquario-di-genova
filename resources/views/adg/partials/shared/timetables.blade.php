<?php
/**
 * Template Name: Fascia Orari di Apertura
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
use App\Post as Post;
if (!isset($item)) {
    $item = (object) [
        'name' => 'timetables',
        'subtitle' => 'Timetables',
        'bodycol1' => 'Body Col 1',
        'bodycol2' => 'Body Col 2',
        'image' => '',
        'link' => null,
        'showbuycta' => false        
    ];
}

$json = file_get_contents("json/months.js");
$months = json_decode($json);

$json = file_get_contents("json/hours.js");
$hours = json_decode($json);

$season = null;

?>
<a name="/{{$item->name}}">&nbsp;</a>
<section class="section-3-columns {{$item->name}}" id="{{$item->name}}">
    @if ($item->image)
    <div class="background wow fadeIn FUP" data-wow-delay="250ms">
        <img src="{{$item->image}}" />
    </div>
    @endif
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="wow fadeIn FUP" data-wow-delay="250ms">
                    <h2>{!!$item->subtitle!!}.</h2>
                </div>
            </div>
            <div class="col-sm-6 col-md-8">
                <div class="row text-justify">
                    <div class="col-md-6">
                        <div class="wow fadeIn FUP" data-wow-delay="450ms">
                            {!!$item->bodycol1!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="wow fadeIn FUP" data-wow-delay="650ms">
                            {!!$item->bodycol1!!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-xs-12">
                <div class="buttons wow fadeIn" data-wow-delay="850ms">
                    @if ($item->link)
                        @foreach ($item->link as $index => $link)
                            @include($link->template, ['link' => $link])
                        @endforeach                        
                    @endif
                    @if (isset($item->showbuycta) && $item->showbuycta)
                        @include('adg.partials.shared.buttons.sticky-transparent')
                    @endif
                </div>
            </div>
        </div>        
        <div class="row">
            <div class="col-sm-12">
                @if ($months && $hours)
                <table class="table table-timetable">
                    <thead>
                        <tr>
                            <th></th>
                            @for ($i = 0; $i < 31; $i++)
                                <th>{{$i+1}}</th>
                            @endfor
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($months as $index => $month)
                        <tr>
                            <td>{{$month->name}}</td>
                            @for ($i = 0; $i < 31; $i++)
                                <td>                                        
                                    @if ($i < count($month->days))
                                        @foreach ($hours as $index => $hour)
                                            @if ($hour->id == $month->days[$i])
                                                <span class="dot {{$hour->type}}-{{$hour->id}}" title="{{$hour->title}}"></span>
                                            @endif
                                        @endforeach
                                    @else
                                        <span class="dot empty"></span>
                                    @endif
                                </td>
                            @endfor
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif
            </div>        
            @foreach ($hours as $index => $hour)   
                @if ($season != $hour->type)
                    </div>
                    <div class="row {{$season = $hour->type}}">    
                @endif                         
                <div class="col-sm-6 col-md-4">
                    <p class="legend">
                        <span class="dot {{$hour->type}}-{{$hour->id}}" title="{{$hour->title}}"></span> <span>{{$hour->title}}</span>
                    </p>
                </div>
            @endforeach
        </div>
    </div>
</section>
