<?php
/**
 * Template Name: Fascia Testo Centrato Con News
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
if (!isset($item)) {
    $item = (object) [
        'name' => 'name',
        'title' => 'Title',
        'subtitle' => 'Subtitle',
        'abstract' => 'Abstract',
        'image' => 'http://placehold.it/1600x720',
        'link' => (object) [
            'template' => 'adg.partials.shared.buttons.secondary',
            'title' => 'Button',
            'uri' => '#'
        ],
        'showbuycta' => true,
        'news' => (object) [
            'name' => 'name',
            'title' => 'Title',
            'image' => 'http://placehold.it/130x130',
            'link' => (object) [
                'template' => 'adg.partials.shared.buttons.accent',
                'title' => 'Read More',
                'uri' => '#'
            ]
        ]
    ];
}
?>
<section class="section-center section-closure {{$item->name}}" id="{{$item->name}}">
    <div class="background wow fadeIn FUP" data-wow-delay="250ms">
        <img src="{{$item->image}}" />
    </div>
    <div class="container-fluid">
        <div class="row text-center">
            <div class="col-sm-12">
                <div class="wow fadeIn FUP" data-wow-delay="250ms">
                    <h3 class="text-serif">{{$item->title}}</h3>
                    <h1>{{$item->subtitle}}</h1>
                </div>
                <div class="wow fadeIn FUP" data-wow-delay="450ms">{!!$item->abstract!!}</div>
                <div class="buttons wow fadeIn" data-wow-delay="650ms">
                    @if ($item->link)
                        @foreach ($item->link as $index => $link)
                            @include($link->template, ['link' => $link])
                        @endforeach                        
                    @endif
                    @if (isset($item->showbuycta) && $item->showbuycta)
                        @include('adg.partials.shared.buttons.sticky-transparent')
                    @endif
                </div>
            </div>
        </div>
        @if (isset($item->news))
        <div class="row">
            <div class="col-sm-6 text-right">
                <div class="wow fadeInLeft" data-wow-delay="650ms">
                    <div class="circle-news"><img src="{{$item->news->image}}" /></div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="wow fadeIn FUP" data-wow-delay="250ms">
                    <h3 class="text-serif">News</h3>
                    <h3 class="text-serif" style="font-size: 40px;">{{$item->news->title}}</h3>
                </div>
                <div class="wow fadeIn FUP" data-wow-delay="450ms">
                    @if ($item->news->link)
                        @foreach ($item->news->link as $index => $link)
                            @include($link->template, ['link' => $link])
                        @endforeach                        
                    @endif
                </div>
            </div>
        </div>
        @endif
    </div>
</section>
