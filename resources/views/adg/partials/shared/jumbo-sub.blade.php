<?php
/**
 * Template Name: Fascia Jumbo Tre Colonne
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
use App\Post as Post;
if (!isset($item)) {
    $item = (object) [
        'name' => 'name',
        'subtitle' => 'Subtitle',
        'bodycol1' => 'Body Col 1',
        'bodycol2' => 'Body Col 2',
        'video' => null,
        'image' => 'http://placehold.it/1600x684',
        'link' => (object) [
            'title' => 'Title',
            'template' => 'adg.partials.shared.buttons.transparent',
            'uri' => '#'
        ]
    ];
}
?>
<section class="jumbo jumbo-sub {{$item->name}}">
    <!--
    @if (isset($item->video))
    <div background class="transition">
        <video src="{{$item->video}}" autoplay loop></video>
    </div>
    @elseif (isset($item->image))
    <div class="background">
        <img src="{{$item->image}}" />
    </div>
    @endif
    -->
    <div class="container-fluid">
        <div class="vertical">
            <div class="center">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="wow fadeIn FUP" data-wow-delay="250ms">
                            <h2>{!!$item->subtitle!!}</h2>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <div class="row text-justify">
                            <div class="col-md-6">
                                <div class="wow fadeIn FUP" data-wow-delay="450ms">
                                    {!!$item->bodycol1!!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="wow fadeIn FUP" data-wow-delay="650ms">
                                    {!!$item->bodycol2!!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>