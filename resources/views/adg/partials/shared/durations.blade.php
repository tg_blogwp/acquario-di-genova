<?php
/**
 * Template Name: Fascia Durata Percorsi
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
use App\Post as Post;
if (!isset($item)) {
    $item = (object) [
        'name' => 'duration',
        'subtitle' => 'Duration',
        'bodycol1' => 'Body Col 1',
        'bodycol2' => 'Body Col 2',
        'image' => '',
        'link' => null,
        'showbuycta' => false        
    ];
}

$json = file_get_contents("json/visits.js");
$visits = json_decode($json);

$json = file_get_contents("json/experiences.js");
$experiences = json_decode($json);

?>
<a name="/{{$item->name}}">&nbsp;</a>
<section class="section-3-columns {{$item->name}}" id="{{$item->name}}">
    @if ($item->image)
    <div class="background wow fadeIn FUP" data-wow-delay="250ms">
        <img src="{{$item->image}}" />
    </div>
    @endif
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="wow fadeIn FUP" data-wow-delay="250ms">
                    <h2>{!!$item->subtitle!!}.</h2>
                </div>
            </div>
            <div class="col-sm-6 col-md-8">
                <div class="row text-justify">
                    <div class="col-md-6">
                        <div class="wow fadeIn FUP" data-wow-delay="450ms">
                            {!!$item->bodycol1!!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="wow fadeIn FUP" data-wow-delay="650ms">
                            {!!$item->bodycol2!!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-xs-12">
                <div class="buttons wow fadeIn" data-wow-delay="850ms">
                    @if ($item->link)
                        @include($item->link->template, ['link' => $item->link])
                    @endif
                    @if (isset($item->showbuycta) && $item->showbuycta)
                        @include('adg.partials.shared.buttons.sticky-transparent')
                    @endif
                </div>
            </div>
        </div>        
        <div class="row">
            <div class="col-sm-12">
                @if ($visits && $experiences)
                <table class="table table-duration">
                    <thead>
                        <tr>
                            <th></th>
                            @foreach ($experiences as $index => $experience)
                            <th>{{$experience->title}}</th>
                            @endforeach      
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($visits as $index => $visit)
                        <tr>
                            <td>{{$visit->title}}</td>
                            @foreach ($experiences as $index => $experience)
                            <td>
                                @if (in_array($visit->id, $experience->visits))
                                    <i class="icon icon-checkmark"></i>
                                @endif
                            </td>
                            @endforeach  
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                        <td>Duration</td>
                        @foreach ($experiences as $index => $experience)
                            <td><span>{{$experience->duration}}</span></td>
                        @endforeach     
                        </tr>
                    </tfoot>
                </table>
                @endif
            </div>
        </div>
    </div>
</section>
