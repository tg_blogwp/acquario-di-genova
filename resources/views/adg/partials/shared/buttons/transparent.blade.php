<?php
/**
 * Template Name: Button Transparent
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
if (!isset($link)) {
	$link = (object) [
	    'title' => 'button',
	    'uri' => '#',	    
	    'target' => '_self'	    
	];
}
?>
<a href="{{$link->uri}}" target="{{$link->target}}" class="btn btn-transparent wow fadeIn FUP" data-wow-delay="300ms" title="{{$link->title}}">
    <span>{{$link->title}}</span>
</a>
