<?php
/**
 * Template Name: Button Secondary
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
if (!isset($link)) {
	$link = (object) [
	    'title' => 'button',
	    'uri' => '#',	    
	    'target' => '_self'    
	];
}
?>
<a href="{{$link->uri}}" target="{{$link->target}}" class="btn btn-secondary" title="{{$link->title}}">
    <span>{{$link->title}}</span>
</a>