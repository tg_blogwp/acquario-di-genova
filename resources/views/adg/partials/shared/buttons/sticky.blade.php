<?php
/**
 * Template Name: Button Sticky
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
if (!isset($link)) {
	$link = (object) [
	    'title' => 'Buy Your Tickets',
	    'uri' => '/plan-your-visit',	    
	    'target' => '_self'	    
	];
}
?>
<div class="sticky-item">
    <div class="sticky-horizontal">
        <a href="{{$link->uri}}" target="{{$link->target}}" class="btn btn-primary btn-sticky" title="{{$link->title}}">
            <i class="icon-tickets"></i> <span>{{$link->title}}</span>
        </a>
    </div>
</div>
