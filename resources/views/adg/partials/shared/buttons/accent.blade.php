<?php
/**
 * Template Name: Button Accent
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
if (!isset($link)) {
	$link = (object) [
	    'title' => 'button',
	    'uri' => '#',	    
	    'target' => '_self'
	];
}
?>
<a href="{{$link->uri}}" target="{{$link->target}}" class="btn btn-accent" title="{{$link->title}}">
    <i class="icon-arrow-right"></i> <span>{{$link->title}}</span>
</a>