<?php
/**
 * Template Name: Fascia Homepage Welcome
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
if (!isset($item)) {
    $item = (object) [
        'name' => 'name',
        'title' => 'Title',
        'subtitle' => 'Subtitle',
        'abstract' => 'Abstract',
        'image' => 'http://placehold.it/1600x800',
        'showbuycta' => true,
        'link' => [
            $link = (object) [
                'title' => 'Title',
                'template' => 'adg.partials.shared.buttons.accent',
                'uri' => '#'
            ],
            $link
        ],
        'items' => [
            $sub = (object) [
                'title' => 'Title',
                'abstract' => 'xxx'
            ],
            $sub
        ]
    ];
}
?>
<section class="{{$item->name}}">
    <div class="background wow fadeIn FUP" data-wow-delay="250ms"><img src="{{$item->image}}" /></div>
    <div class="container-fluid">
        <div class="row text-center">
            <div class="col-sm-12">
                <div class="wow fadeIn FUP" data-wow-delay="250ms">
                    <h3 class="text-serif">{{$item->title}}</h3>
                    <h1>{{$item->subtitle}}</h1>
                </div>
                <div class="wow fadeIn FUP" data-wow-delay="450ms">
                    <p>{!!$item->abstract!!}</p>
                </div>
                <div class="wow fadeIn FUP" data-wow-delay="650ms">
                    @foreach ($item->link as $link)
                    @include($link->template, ['link' => $link])
                    @endforeach
                </div>
            </div>
        </div>
        @if (count($item->items) > 0)
        <div class="row" style="padding-top: 120px;">
            @foreach ($item->items as $index => $sub)
            <div class="col-xs-6 col-md-3 text-center">
                <div class="circle wow fadeIn" data-wow-delay="{{(150 * $index)}}ms">
                    <div class="before" style="content: url('img/circle.svg');"></div>
                    <div class="vertical">
                        <div class="center">
                            <b>{{$sub->abstract}}</b>
                            <p>{{$sub->title}}</p>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        @if (isset($item->showbuycta) && $item->showbuycta)
        <div class="row">
            <div class="col-sm-12">
                <div class="text-center padding-top-1">
                    @include('adg.partials.shared.buttons.sticky-transparent')
                </div>
            </div>
        </div>
        @endif
    </div>
</section>
