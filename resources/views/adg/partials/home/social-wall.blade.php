<?php
/**
 * Template Name: Fascia Homepage Social Wall
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
 ?>
 <section class="follow">
    <div class="row">
        <div class="col-sm-12 text-center">
            <div class="wow fadeIn FUP">
                <h3 class="text-serif" style="font-size: 40px; margin: 0;">Follow Us</h3>
                <br />
                <ul class="menu text-center" role="navigation" style="display: inline-block; margin-bottom: 40px;">
                    <li>
                        <a href="#">
                            <span>Facebook</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span>Twitter</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span>Youtube</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span>Instagram</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span>Flickr</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span>Google</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-6 col-sm-4">
                <a href="#" class="box wow fadeIn" data-wow-delay="0ms">
                    <span class="media">
                        <span class="picture aspect ratio1x1">
                            <img src="img/social-01.jpg" class="inner" />
                            <span class="overlay"></span>
                        </span>
                    </span>
                    <span class="headline-top">
                        <i class="icon-instagram"></i>
                        10 DAYS AGO
                    </span>
                    <span class="headline-bottom hidden-xs">
                        <span class="title text-serif">Genova</span>
                        <span class="description text-serif">
                            <em>#me #aquarium #acquario #genova #fun #vacation #weekend #italy #italian #italia #dolphins #delfini #acquariodigenova</em>
                        </span>
                    </span>
                </a>
            </div>
            <div class="col-xs-12 col-sm-8">
                <a href="#" class="box wow fadeIn" data-wow-delay="150ms">
                    <span class="media">
                        <span class="picture aspect ratio8cx4c">
                            <img src="img/social-02.jpg" class="inner" />
                            <span class="overlay"></span>
                        </span>
                    </span>
                    <span class="headline-top">
                        <i class="icon-twitter"></i>
                        10 DAYS AGO
                    </span>
                    <span class="headline-bottom">
                        <span class="name">Angel Jose Mentefr�a</span>
                        <span class="title">Es perfecto para cenar. Es una sensaci�n �nica. Te imaginas que eres Neptuno y ella una sirena y con imaginaci�n puede ser fascinante</span>
                        <span class="description text-serif">
                            <em>#genova #italy #food #neptuno #acquariodigenova</em>
                        </span>
                    </span>
                </a>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-xs-6 col-sm-4">
                <a href="#" class="box wow fadeIn" data-wow-delay="300ms">
                    <span class="media">
                        <span class="picture aspect ratio1x1">
                            <img src="img/social-03.jpg" class="inner" />
                            <span class="overlay"></span>
                        </span>
                    </span>
                    <span class="headline-bottom">
                        <i class="icon-flickr"></i>
                        11 DAYS AGO
                    </span>
                </a>
            </div>
            <div class="col-xs-6 col-sm-4">
                <a href="#" class="box wow fadeIn" data-wow-delay="450ms">
                    <span class="media">
                        <span class="picture aspect ratio1x1">
                            <img src="img/social-04.jpg" class="inner" />
                            <span class="overlay"></span>
                        </span>
                        <span class="play">
                            <div class="vertical">
                                <div class="center">
                                    <i class="icon-play"></i>
                                </div>
                            </div>
                        </span>
                    </span>
                    <span class="headline-bottom">
                        <i class="icon-instagram"></i>
                        11 DAYS AGO
                    </span>
                </a>
            </div>
            <div class="col-xs-6 col-sm-4">
                <a href="#" class="box wow fadeIn" data-wow-delay="600ms">
                    <span class="media">
                        <span class="picture aspect ratio1x1">
                            <img src="img/social-05.jpg" class="inner" />
                            <span class="overlay"></span>
                        </span>
                    </span>
                    <span class="headline-top">
                        <i class="icon-twitter"></i>
                        12 DAYS AGO
                    </span>
                    <span class="headline-bottom hidden-xs">
                        <span class="title text-serif">Dentist visit</span>
                        <span class="description text-serif">
                            <em>#genova #funny #italia #acquariodigenova</em>
                        </span>
                    </span>
                </a>
            </div>
        </div>
    </div>
</section>
