<?php
/**
 * Template Name: Fascia Homepage Spots
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
if (!isset($item)) {
    $item = (object) [
        'name' => 'name',
        'items' => [
            $spot = (object) [
                'title' => 'Title',
                'abstract' => 'Abstract',
                'image' => 'http://placehold.it/223x186',
                'link' => '#'
            ],
            $spot,
            $spot,
            $spot
        ]
    ];
}
?>
@if ($item->items && count($item->items) > 0)
<section class="spots {{$item->name}}">
    <div class="container-fluid">
        <div class="row">
            <div class="slick-gallery" slick slick-options="slickOptions.spots" arrows="false">
                @foreach ($item->items as $index => $sub)
                <div class="col-sm-6 col-md-3">
                    <a href="{{$sub->link}}" class="box wow fadeIn" data-wow-delay="{{(150 * $index)}}ms" data-wow-offset="-400">
                        <span class="media">
                            <span class="picture">
                                <img src="{{$sub->image}}" />
                                <span class="overlay"></span>
                            </span>
                        </span>
                        <span class="headline-bottom">
                            <span class="title">{!!$sub->title!!}</span>
                            <span class="description">{{$sub->abstract}}</span>
                        </span>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
@endif