<?php
/**
 * Template Name: Fascia Homepage Chiusura
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
if (!isset($item)) {
    $item = (object) [
        'name' => 'name',
        'title' => 'Title',
        'subtitle' => 'Subtitle',
        'image' => 'http://placehold.it/1600x720'
    ];
}
?>
<section class="closure {{$item->name}}">
    <div class="background wow fadeIn FUP" data-wow-delay="250ms">
        <img src="{{$item->image}}" />
    </div>
    <div class="container-fluid">
        <div class="row text-center">
            <div class="col-sm-12">
                <div class="wow fadeIn FUP" data-wow-delay="250ms">
                    <div class="vertical" style="width: 100%; height: 480px;">
                        <div class="center">
                            <q>
                                {{$item->title}}
                                <br />
                                {{$item->subtitle}}
                            </q>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
