<?php
/**
 * Template Name: Informazioni
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */

// $fields = get_fields($post[0]->ID);
// $json = file_get_contents("json/info.js");
// $page = json_decode($json);

/*
switch (json_last_error()) {
    case JSON_ERROR_NONE:
        echo ' - No errors';
    break;
    case JSON_ERROR_DEPTH:
        echo ' - Maximum stack depth exceeded';
    break;
    case JSON_ERROR_STATE_MISMATCH:
        echo ' - Underflow or the modes mismatch';
    break;
    case JSON_ERROR_CTRL_CHAR:
        echo ' - Unexpected control character found';
    break;
    case JSON_ERROR_SYNTAX:
        echo ' - Syntax error, malformed JSON';
    break;
    case JSON_ERROR_UTF8:
        echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
    break;
    default:
        echo ' - Unknown error';
    break;
}
*/
?>
@extends('adg.layouts.master', ['page' => $page])

@section('header')
    @include('adg.partials.shared.jumbo-sub', ['item' => $page])
    @include('adg.partials.shared.sections', ['item' => $page]) 
    @include('adg.partials.shared.header')
@endsection

@section('content-class', 'light')

@section('content')    
    @if (isset($page->items))
        @foreach ($page->items as $index => $sub)
            @include($sub->template, ['item' => $sub])
        @endforeach
    @endif
@endsection

@section('footer')
    @include('adg.partials.shared.footer')
@endsection