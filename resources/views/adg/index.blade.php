<?php
/**
 * Template Name: Index
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */
$json = file_get_contents("json/index.js");
$page = json_decode($json);
?>
@extends('adg.layouts.master', ['page' => $page])

@section('header')
    @include('adg.partials.shared.jumbo', ['item' => $page])
    @include('adg.partials.shared.hours', ['item' => $page])
    @include('adg.partials.shared.header')    
@endsection

@section('content')
    @if (isset($page->items))
        @foreach ($page->items as $index => $sub)
            @include($sub->template, ['item' => $sub])
        @endforeach
    @endif
@endsection

@section('footer')
    @include('adg.partials.shared.footer')
@endsection