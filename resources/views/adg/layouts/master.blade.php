<?php
/**
 * Template Name: Layout Master
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */

/*
use App\Post as Post;
use App\Page as Page;
use App\Nav as Nav;

$posts = Post::published()->get();
$pages = Page::published()->get();
$navs = Nav::published()->get();
*/

?>
<!DOCTYPE html>
<html <?=language_attributes()?> class="font-observer" xmlns:fb="http://ogp.me/ns/fb#">
<!-- manifest="site-appcache.txt">-->
<head prefix="og:http://ogp.me/ns#">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="<?=bloginfo('charset')?>">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?=bloginfo('pingback_url')?>">
    <?=wp_head()?>
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-title" content="Acquario di Genova, the largest aquarium in Europe">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="The official website of Acquario di Genova offers a virtual visit to one of the most beautiful exhibition facilities in Italy." />
    <meta name="robots" content="noodp" />
    <link rel="canonical" href="http://www.acquariodigenova.it/en/" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Acquario di Genova, the largest aquarium in Europe" />
    <meta property="og:description" content="The official website of Acquario di Genova offers a virtual visit to one of the most beautiful exhibition facilities in Italy." />
    <meta property="og:url" content="http://www.acquariodigenova.it/en/" />
    <meta property="og:site_name" content="Acquario di Genova" />
    <style>
        [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
            display: none !important;
        }
    </style>
    <link rel="shortcut icon" href="img/favicon.jpg">
    <link href="/css/vendors.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">
    <link href="/fonts/fonts.css" rel="stylesheet">
    <!--<link href="/css/grid.css" rel="stylesheet">-->
    <script>
    var sections = [
    @if (isset($page) && $page->items)
        @foreach ($page->items as $index => $sub)
        @if (isset($sub->label))
            {
                name: "{{$sub->name}}",
                label: "{{$sub->label}}",
            },
        @endif
        @endforeach
    @endif
    ];
    </script>
</head>
<!-- <?=body_class()?> -->
<body class="{{$page->name}}" ng-app="acquario" smooth-scroll>    
    <div class="wrapper-xl transition" ng-class="{ blur: menuMobileOpened }">
        <header sticky-header=".layer">
            @yield('header')
        </header>
        <div id="content" class="content @yield('content-class', 'dark')">
            @yield('content')
        </div>
        <footer>
            @yield('footer')
        </footer>
    </div>
    <script src="/js/vendors.js"></script>
    <script src="/js/main.js"></script>
    <link href="https://file.myfontastic.com/qtVwweDSjgF5A5PKF9WG8P/icons.css" rel="stylesheet">

    <div class="dump" ng-class="{ active: dump }" ng-click="dump=!dump">
        <b ng-bind="dump ? 'close' : 'show dump'"></b>
        <div ng-if="dump">
            <h3>Page</h3>
            {{var_dump($page)}}
        </div>
    </div>

    {{--
    <div class="dump" ng-class="{ active: dump }" ng-click="dump=!dump">
        <b ng-bind="dump ? 'close' : 'show dump'"></b>
        <div ng-if="dump">
            <h3>Posts</h3>
            @foreach ($posts as $key => $value)
                {{var_dump($value->map())}}
            @endforeach

            <h3>Pages</h3>
            @foreach ($pages as $key => $value)
                {{var_dump($value->map())}}
            @endforeach

            <h3>Navs</h3>
            @foreach ($navs as $key => $value)
                {{var_dump($value->map())}}
            @endforeach
        </div>
    </div>
    --}}

</body>
</html>
