<?php
/**
 * Template Name: Experience
 *
 * @package WordPress
 * @subpackage Adg
 * @since Adg 1.0
 * @author Websolute
 */


if(is_array($page->items)){

    foreach($page->items as $item){

        if(is_array($item->struttura)){

            $strutturaParam = get_fields($item->struttura[0]);
            $item->name = $strutturaParam['name'];
            $item->label = $strutturaParam['label'];
            $item->title = $strutturaParam['title'];
            $item->subtitle = $strutturaParam['subtitle'];
            $item->description = $strutturaParam['hero'];
            $item->abstract = $strutturaParam['abstract'];

        }
        
    }

}

?>
@extends('adg.layouts.master', ['item' => $page])

@section('header')
    @include('adg.partials.shared.jumbo', ['item' => $page])
    @include('adg.partials.shared.hours')
    @include('adg.partials.shared.jumbo-sub', ['item' => $page])
    @include('adg.partials.shared.sections', ['item' => $page]) 
    @include('adg.partials.shared.header')
@endsection

@section('content-class', 'light')

@section('content')
    @if(is_array($page->items))
	    @foreach ($page->items as $index => $sub)
    	    @include($sub->template, ['item' => $sub])
        @endforeach
    @endif
@endsection

@section('footer')
    @include('adg.partials.shared.footer')
@endsection