<!-- Stored in resources/views/welcome.blade.php -->
@extends('layouts.master')

@section('header')
    @include('adg.partials.shared.header')
@endsection

@section('content')
<div class="title">Laravel 5 for websolute</div>
<?php
$adgSoap = new \App\Http\Controllers\SoapController();
$adgSoap->demo();
?>
@endsection

@section('footer')
    @include('adg.partials.shared.footer')
@endsection
