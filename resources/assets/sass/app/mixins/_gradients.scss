﻿// Gradients

    /// Convert angle
    /// @author Chris Eppstein
    /// @param {Number} $value - Value to convert
    /// @param {String} $unit - Unit to convert to
    /// @return {Number} Converted angle
    @function convert-angle($value, $unit) {
      $convertable-units: deg grad turn rad;
      $conversion-factors: 1 (10grad/9deg) (1turn/360deg) (3.1415926rad/180deg);
      @if index($convertable-units, unit($value)) and index($convertable-units, $unit) {
        @return $value
                 / nth($conversion-factors, index($convertable-units, unit($value)))
                 * nth($conversion-factors, index($convertable-units, $unit));
      }
  
      @warn "Cannot convert `#{unit($value)}` to `#{$unit}`.";
    }

    /// Test if `$value` is an angle
    /// @param {*} $value - Value to test
    /// @return {Bool}
    @function is-direction($value) {
      $is-direction: index((to top, to top right, to right top, to right, to bottom right, to right bottom, to bottom, to bottom left, to left bottom, to left, to left top, to top left), $value);
      $is-angle: type-of($value) == 'number' and index('deg' 'grad' 'turn' 'rad', unit($value));
  
      @return $is-direction or $is-angle;
    }

    /// Convert a direction to legacy syntax
    /// @param {Keyword | Angle} $value - Value to convert
    /// @require {function} is-direction
    /// @require {function} convert-angle
    @function legacy-direction($value) {
      @if is-direction($value) == false {
        @warn "Cannot convert `#{$value}` to legacy syntax because it doesn't seem to be an angle or a direction";
      }
  
      $conversion-map: ( 
        to top          : bottom,
        to top right    : bottom left,
        to right top    : left bottom,
        to right        : left,
        to bottom right : top left,
        to right bottom : left top,
        to bottom       : top,
        to bottom left  : top right,
        to left bottom  : right top,
        to left         : right,
        to left top     : right bottom,
        to top left     : bottom right
      );
    
      @if map-has-key($conversion-map, $value) {
        @return map-get($conversion-map, $value);
      }
  
      @return 90deg - convert-angle($value, 'deg');
    }

    /// Mixin printing a linear-gradient 
    /// as well as a plain color fallback 
    /// and the `-webkit-` prefixed declaration
    /// @access public
    /// @param {String | List | Angle} $direction - Linear gradient direction
    /// @param {Arglist} $color-stops - List of color-stops composing the gradient
    @mixin linear-gradient($direction, $color-stops...) {
      @if is-direction($direction) == false {
        $color-stops: ($direction, $color-stops);
        $direction: 180deg;
      }
  
      background: nth(nth($color-stops, 1), 1);
      background: -webkit-linear-gradient(legacy-direction($direction), $color-stops);
      background: linear-gradient($direction, $color-stops);
    }

  // Horizontal gradient, from left to right
  //
  // Creates two color stops, start and end, by specifying a color and position for each color stop.
  // Color stops are not available in IE9 and below.
  @mixin gradient-horizontal($start-color: #555, $end-color: #333, $start-percent: 0%, $end-percent: 100%) {
    background-image: -webkit-linear-gradient(left, $start-color $start-percent, $end-color $end-percent); // Safari 5.1-6, Chrome 10+
    background-image: -o-linear-gradient(left, $start-color $start-percent, $end-color $end-percent); // Opera 12
    background-image: linear-gradient(to right, $start-color $start-percent, $end-color $end-percent); // Standard, IE10, Firefox 16+, Opera 12.10+, Safari 7+, Chrome 26+
    background-repeat: repeat-x;
    filter: e(%("progid:DXImageTransform.Microsoft.gradient(startColorstr='%d', endColorstr='%d', GradientType=1)",argb($start-color),argb($end-color))); // IE9 and down
  }

  // Vertical gradient, from top to bottom
  //
  // Creates two color stops, start and end, by specifying a color and position for each color stop.
  // Color stops are not available in IE9 and below.
  @mixin gradient-vertical($start-color: #555, $end-color: #333, $start-percent: 0%, $end-percent: 100%) {
    background-image: -webkit-linear-gradient(top, $start-color $start-percent, $end-color $end-percent);  // Safari 5.1-6, Chrome 10+
    background-image: -o-linear-gradient(top, $start-color $start-percent, $end-color $end-percent);  // Opera 12
    background-image: linear-gradient(to bottom, $start-color $start-percent, $end-color $end-percent); // Standard, IE10, Firefox 16+, Opera 12.10+, Safari 7+, Chrome 26+
    background-repeat: repeat-x;
    filter: e(%("progid:DXImageTransform.Microsoft.gradient(startColorstr='%d', endColorstr='%d', GradientType=0)",argb($start-color),argb($end-color))); // IE9 and down
  }

  @mixin gradient-directional($start-color: #555, $end-color: #333, $deg: 45deg) {
    background-repeat: repeat-x;
    background-image: -webkit-linear-gradient($deg, $start-color, $end-color); // Safari 5.1-6, Chrome 10+
    background-image: -o-linear-gradient($deg, $start-color, $end-color); // Opera 12
    background-image: linear-gradient($deg, $start-color, $end-color); // Standard, IE10, Firefox 16+, Opera 12.10+, Safari 7+, Chrome 26+
  }
  @mixin gradient-horizontal-three-colors($start-color: #00b3ee, $mid-color: #7a43b6, $color-stop: 50%, $end-color: #c3325f) {
    background-image: -webkit-linear-gradient(left, $start-color, $mid-color $color-stop, $end-color);
    background-image: -o-linear-gradient(left, $start-color, $mid-color $color-stop, $end-color);
    background-image: linear-gradient(to right, $start-color, $mid-color $color-stop, $end-color);
    background-repeat: no-repeat;
    filter: e(%("progid:DXImageTransform.Microsoft.gradient(startColorstr='%d', endColorstr='%d', GradientType=1)",argb($start-color),argb($end-color))); // IE9 and down, gets no color-stop at all for proper fallback
  }
  @mixin gradient-vertical-three-colors($start-color: #00b3ee, $mid-color: #7a43b6, $color-stop: 50%, $end-color: #c3325f) {
    background-image: -webkit-linear-gradient($start-color, $mid-color $color-stop, $end-color);
    background-image: -o-linear-gradient($start-color, $mid-color $color-stop, $end-color);
    background-image: linear-gradient($start-color, $mid-color $color-stop, $end-color);
    background-repeat: no-repeat;
    filter: e(%("progid:DXImageTransform.Microsoft.gradient(startColorstr='%d', endColorstr='%d', GradientType=0)",argb($start-color),argb($end-color))); // IE9 and down, gets no color-stop at all for proper fallback
  }
  @mixin gradient-radial($inner-color: #555, $outer-color: #333) {
    background-image: -webkit-radial-gradient(circle, $inner-color, $outer-color);
    background-image: radial-gradient(circle, $inner-color, $outer-color);
    background-repeat: no-repeat;
  }
  @mixin gradient-striped($color: rgba(255,255,255,.15), $angle: 45deg) {
    background-image: -webkit-linear-gradient($angle, $color 25%, transparent 25%, transparent 50%, $color 50%, $color 75%, transparent 75%, transparent);
    background-image: -o-linear-gradient($angle, $color 25%, transparent 25%, transparent 50%, $color 50%, $color 75%, transparent 75%, transparent);
    background-image: linear-gradient($angle, $color 25%, transparent 25%, transparent 50%, $color 50%, $color 75%, transparent 75%, transparent);
  }

