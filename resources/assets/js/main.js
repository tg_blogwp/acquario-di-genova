﻿/*global dynamics*/

var app = angular.module('acquario', ['ngRoute']);

app.constant('ROUTES', window.sections ? sections : []);
app.constant('VISITS', window.visits ? visits : []);
app.constant('EXPERIENCES', window.experiences ? experiences : []);
app.constant('FARES', window.fares ? fares : []);

app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    
    $routeProvider.when('/:section', {
        // DEFINE ROUTE
    }).when('/:section/:chapter', {
        // DEFINE ROUTE
    });

    // $routeProvider.otherwise('/404');

    // HTML5 MODE url writing method (false: #/anchor/use, true: /html5/url/use)
    $locationProvider.html5Mode(false);

}]);

app.run(['$rootScope', 'ROUTES', 'EXPERIENCES', function ($rootScope, ROUTES, EXPERIENCES) {

    $rootScope.$on('$routeChangeSuccess', function ($e, current, previous) {
        // console.log('$routeChangeSuccess', current.params);
        $rootScope.section = current.params.section;
        $rootScope.chapter = current.params.chapter;
        angular.forEach(ROUTES, function (value, index) {
            if (value.name == $rootScope.section) {
                value.chapter = $rootScope.chapter;
            }
        });
    });

    $rootScope.isBeforeActiveIndex = function ($index) {
        var is = false;
        angular.forEach(ROUTES, function (value, index) {
            // console.log(value, $rootScope.section, index, $index);
            if (value.name == $rootScope.section && index > $index) {
                is = true;
            }
        });
        return is;
    };

    $rootScope.isSubActive = function ($section, $chapter, $index) {
        if ($rootScope.section == $section && $rootScope.chapter == $chapter) {
            return true;
        } else {
            var is = false;
            angular.forEach(ROUTES, function (value, index) {
                if (value.name == $section) {
                    is = (value.chapter ? value.chapter == $chapter : $index == 0);
                }
            });
            return is;
        }
    };

    $rootScope.slickOptions = {
        spots: {
            dots: true,
            arrows: false,
            draggable: true,
            autoplay: false,
            infinite: false,
            lazyLoad: 'progressive',
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [{
                breakpoint: 1023,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    variableWidth: true
                }
            }]
        },
        more: {
            dots: false,
            arrows: true,
            draggable: true,
            autoplay: false,
            infinite: true,
            lazyLoad: 'progressive',
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [{
                breakpoint: 1023,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
        }
    };

    $rootScope.broadcast = function (event, params) {
        $rootScope.$broadcast(event, params);
    }
    $rootScope.menuMobileOpen = function () {
        $rootScope.menuMobileOpened = true;
    };
    $rootScope.menuMobileClose = function () {
        $rootScope.menuMobileOpened = false;
    };
    $rootScope.menuOpen = function () {
        $rootScope.menuOpened = true;
    };
    $rootScope.menuClose = function () {
        $rootScope.menuOpened = false;
    };
    $rootScope.menuToggle = function () {
        $rootScope.menuOpened = !$rootScope.menuOpened;
    };
    $rootScope.gotoTickets = function () {
        window.location.href = 'tickets.html';
    };

    $rootScope.doShowExperience = function (dir) {
        if (dir > 0) {
            $rootScope.experienceIndex ++;
            if ($rootScope.experienceIndex >= EXPERIENCES.length) {
                $rootScope.experienceIndex = 0;
            }
        } else if (dir < 0) {
            $rootScope.experienceIndex --;
            if ($rootScope.experienceIndex < 0) {
                $rootScope.experienceIndex = EXPERIENCES.length - 1;
            }
        } else {
            $rootScope.experienceIndex = 0;
        }
    };
    $rootScope.isExperienceVisible = function (index) {
        return $rootScope.experienceIndex === index;
    };

    $rootScope.doBuyTicket = function (id) {
        alert('doBuyTicket ' + id);
    };

    /*
    // FONT PRELOADER
    setTimeout(function () {

        var font400 = new FontFaceObserver("Proxima Nova", {
            weight: 400
        });
        var font500 = new FontFaceObserver("Proxima Nova", {
            weight: 500
        });
        var font600 = new FontFaceObserver("Proxima Nova", {
            weight: 600
        });
        var font700 = new FontFaceObserver("Proxima Nova", {
            weight: 700
        });
        var font900 = new FontFaceObserver("Proxima Nova", {
            weight: 900
        });
        var font400Serif = new FontFaceObserver("Adelle", {
            weight: 400
        });
        Promise.all([
            font400.check(),
            font500.check(null, 1000),
            font600.check(null, 1000),
            font700.check(null, 1000),
            font900.check(null, 1000),
            font400Serif.check(),
        ]).then(function () {

            initWowJs();

            setTimeout(function () {
                document.documentElement.className += " fonts-loaded";
            }, 1);

        }, function () {
            document.documentElement.className += " fonts-timeout";
        });

    }, 0);

    */

    function initWowJs() {

        // WOW ANIMATION
        new WOW({
            boxClass: 'wow',
            animateClass: 'animated',
            mobile: false,
            live: true,
            offset: 0,

        }).init();

    };

    initWowJs();

}]);
