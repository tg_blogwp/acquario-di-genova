var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss');
});

elixir(function (mix) {
    mix.styles([
        "animate.css/animate.css",
        // "slick-carousel/slick/slick.css"

    ], 'public/css/vendors.css', 'resources/assets/vendors');
});

elixir(function (mix) {
    mix.scripts([
        "fontFaceObserver.js",
        "smoothscroll.js",
        "wow.js",
        "dynamics.js/lib/dynamics.js",
        "jquery/dist/jquery.js",
        "slick-carousel/slick/slick.js",
        "angular/angular.js",
        "angular-route/angular-route.js"
        
    ], 'public/js/vendors.js', 'resources/assets/vendors')
    .scripts([
        "main/utils.js",
        "main.js",
        "main/directives.js"

    ], 'public/js/main.js', 'resources/assets/js');
});